# Overview
Using the [Advent of Code 2019](https://adventofcode.com/2019) as an excuse to
get better at Rust.

FWIW, I've got a very rudimentary understanding of Rust, having built some
command-line apps and some hacky-multi-threaded data processing scripts.  I
still need to search for how to do almost everything in Rust.

# Run
After installing rust and cloning this repo, change directory into the project
root and run:
```sh
cargo run --bin day02 --release
```

There is some [generated documentation](https://romanows.gitlab.io/advent-of-code-2019)
but it's mostly autogenerated from the source without much extra written
documentation.

# Notes

## Rust

### Pain Point -- Arithmetic with different integer types
It's great that I can be precise as to what type of integer primitive different
variables will hold.  However, when I want to do arithmetic on everything, I've
gotten bogged down in type conversions where I have to cast variables to their
signed/unsigned, larger/smaller, or sized/usized versions to make the
arithmetic work out.  Especially for the advent of code, I just want the
arithmetic to happen and panic if it doesn't work out.  It's not clear to a
n00b, and not completely clear to me yet, when to use `as TYPE`,
`TYPE::from/try\_from()`, `try\_into().unwrap()`, how to write functions that
are generic over their types, or perhaps when to create appropriately-typed
versions of whatever I'm operating on so potentially complicated arithmetic can
remain clear of type-conversion noise.  This is probably a guide waiting to be
written...

### Pain Point -- Compile code under test
Sometimes I'm in the middle of fixing a tremendous number of bugs and want to
run a test for a simple function.  However, `cargo test test_simple_function`
will not work if the entire project cannot compile, as far as I can tell?
Necessitating a lot of commenting-out or working-around other compilation
errors that may be present.

### Pain Point -- Show the top few errors
When there are a lot of bugs in my code, it's a pain to have to scroll up to
fix them all.  It'd be nice to have the following options:

* reverse -- shows all build problems but displays the first build problems last
* top-N -- shows only the first-N build problems
* filter -- shows build problems in a function or some other scope (used when "fixing" one scope or when "fixing" one error may lead to other new errors in the neighborhood)
* lines -- range of lines from which to show errors (used as an alternate way to specify scope when doing that may be easier)

### Pain Point -- Behavior needing traits in scope
The whole traits adding behavior to structs defined elsewhere is confusing; how
can you tell where the behavior comes from?
