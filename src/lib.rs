//! Shared functionality between solutions for different days of [Advent of
//! Code 2019](https://adventofcode.com/2019).

pub mod intcode;
