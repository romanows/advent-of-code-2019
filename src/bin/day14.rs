//! Solution to [Advent of Code 2019 Day 14](https://adventofcode.com/2019/day/14).

extern crate nom;
extern crate petgraph;

use itertools::Itertools;
use petgraph::algo;
use petgraph::graphmap::{DiGraphMap};
use std::collections::HashMap;
use std::env::args_os;
use std::fs::File;
use std::io::{BufReader, BufRead, Write};

#[derive(Debug, Eq, PartialEq)]
pub struct Term {
    pub qty: u64,
    pub chem: String,
}

impl std::fmt::Display for Term {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.qty, self.chem)
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct Reaction {
    pub inputs: Vec<Term>,
    pub output: Term,
}

impl std::fmt::Display for Reaction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} => {}", self.inputs.iter().map(|x| x.to_string()).join(", "), self.output)
    }
}

/// Nom parsers to parse reactions of form "QUANTITY1 CHEMICAL1, QUANTITY2
/// CHEMICAL2 => QUANTITY3 CHEMICAL3".  The tutorial at
/// <https://github.com/benkay86/nom-tutorial> was indepsensable for getting this
/// to work and for improvements in code quality and organization.
pub(self) mod reaction_parser {
    use super::{Term, Reaction};

    /// Quantity of a reaction chemical
    fn quantity(x: &str) -> nom::IResult<&str, u64> {
        match nom::character::complete::digit1(x) {
            Ok((remaining_input, x)) => Ok((remaining_input, x.parse::<u64>().unwrap())),
            Err(e) => Err(e),
        }
    }

    /// Reaction chemical
    fn chemical(x: &str) -> nom::IResult<&str, &str> {
        nom::character::complete::alphanumeric1(x)  // TODO: not correct, but correct enough for toy problem and too much of a nom newbie to do this correctly
    }

    /// Quantity and chemical
    fn term(x: &str) -> std::result::Result<(&str, Term), nom::Err<(&str, nom::error::ErrorKind)>> {
        match nom::sequence::tuple((
            quantity,
            nom::character::complete::space1,
            chemical,
        ))(x) {
            Ok((remaining_input, (qty, _, chem))) => Ok((remaining_input, Term { qty, chem: chem.to_string() })),
            Err(e) => Err(e),
        }
    }

    /// One or more Terms
    fn terms(x: &str) -> std::result::Result<(&str, std::vec::Vec<Term>), nom::Err<(&str, nom::error::ErrorKind)>> {
        nom::multi::separated_nonempty_list(
            nom::multi::many1(nom::character::complete::one_of(", \n\r")),
            term,
        )(x)
    }

    /// The full reaction
    fn reaction(x: &str) -> std::result::Result<(&str, Reaction), nom::Err<(&str, nom::error::ErrorKind)>> {
        match nom::sequence::tuple((
            terms,
            nom::bytes::complete::tag(" => "),
            terms,
        ))(x) {
            Ok((remaining_input, (inputs, _, mut outputs))) => Ok((remaining_input, Reaction { inputs, output: outputs.pop().unwrap() })),
            Err(e) => Err(e),
        }
    }

    /// Error indicating a problem during parsing of a Reaction
    #[derive(Default, PartialEq)]
    pub struct ParseError;

    impl std::fmt::Display for ParseError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "A parsing error occurred.")
        }
    }

    impl std::fmt::Debug for ParseError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            <ParseError as std::fmt::Display>::fmt(self, f)
        }
    }

    impl std::error::Error for ParseError { }

    /// Parses one reaction
    pub fn parse(line: &str) -> std::result::Result<Reaction, ParseError> {
        match reaction(line) {
            Ok((_, x)) => Ok(x),
            Err(_) => Err(ParseError { }),
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_quantity() {
            assert_eq!(quantity("123"), Ok(("", 123u64)));
        }

        #[test]
        fn test_term() {
            assert_eq!(term("123 ABC"), Ok(("", Term { qty: 123u64, chem: "ABC".to_string() } )));
        }

        #[test]
        fn test_terms() {
            assert_eq!(terms("123 ABC"), Ok(("", vec![ Term { qty: 123u64, chem: "ABC".to_string() } ])));
            assert_eq!(terms("123 ABC, 456 DEF"), Ok(("", vec![ Term { qty: 123u64, chem: "ABC".to_string() }, Term { qty: 456u64, chem: "DEF".to_string() } ])));
        }

        #[test]
        fn test_reaction() {
            assert_eq!(reaction("123 ABC, 456 DEF => 789 GHI"), Ok(("", Reaction { inputs: vec![ Term { qty: 123u64, chem: "ABC".to_string() }, Term { qty: 456u64, chem: "DEF".to_string() } ], output: Term { qty: 789u64, chem: "GHI".to_string() } } )));
        }

        #[test]
        fn test_parse() {
            assert_eq!(parse("123 ABC, 456 DEF => 789 GHI"), Ok(Reaction { inputs: vec![ Term { qty: 123u64, chem: "ABC".to_string() }, Term { qty: 456u64, chem: "DEF".to_string() } ], output: Term { qty: 789u64, chem: "GHI".to_string() } } ));
            assert_eq!(parse("123 ABC, 456 DEF => 789x GHI"), Err(ParseError { }));
        }
    }
}

// TODO: Sadly getting lost in a morass of incompatible error types and
// handling errors.  Just going to panic until I have time to really dig into
// all of this.

fn order_reactions(reactions: &Vec<Reaction>) -> Vec<String> {
    // Need to expand the reaction terms that won't be used to produce other
    // reaction terms.  Topo sort and then working backwards from the FUEL end
    // should do this.
    let mut graph: DiGraphMap<&str, Option<&str>> = DiGraphMap::new();
    for reaction in reactions {
        for input in &reaction.inputs {
            let p = graph.add_node(&reaction.output.chem);
            let q = graph.add_node(&input.chem);
            graph.add_edge(p, q, None);
        }
    }
    let topo_sort = algo::toposort(&graph, None);
    match topo_sort {
        Ok(x) => x.iter().map(|x| x.to_string()).collect(),  // just going to copy these references to strings in the reactions structs to simplify memory management
        _ => panic!("error sorting reactions, probably cycle"),
    }
}

fn accumulate_reaction_inputs<'a>(chem_to_count: &mut HashMap<&'a str, u64>, reaction: &'a Reaction) {
    // Amount of the chemical we're attempting to produce that's already present in
    // the reactor
    let output_state_qty = chem_to_count.remove(reaction.output.chem.as_str()).unwrap();

    // We want to account for all of the output quantity in the reaction, so we'll
    // use enough of the inputs to produce as much or a bit more of the output
    // quantity.
    let mut mult = output_state_qty / reaction.output.qty;
    mult += if output_state_qty % reaction.output.qty > 0 { 1 } else { 0 };  // "a bit more", see comment above

    for input in &reaction.inputs {
        // Note: so this inserts a reference to the String in reaction.output into the
        // hashmap, so the reaction needs to live at least as long as the Hashmap.
        *chem_to_count.entry(input.chem.as_str()).or_insert(0) += mult * input.qty;
    }
}

fn calc_min_ore(reactions: &Vec<Reaction>, fuel_qty: u64) -> u64 {
    let ordered_reactions = order_reactions(reactions);

    let output_to_reactions: HashMap<&str, &Reaction> = reactions.iter().map(|x| (x.output.chem.as_str(), x)).collect();

    let mut chem_to_count: HashMap<&str, u64> = HashMap::new();
    chem_to_count.insert("FUEL", fuel_qty);  // initialize the backwards reaction search state

    // get first chemical in sorted? chemicals and attempt to replace it.  Keep doing that until you've only got "ORE" in the map.
    while chem_to_count.keys().any(|k| *k != "ORE") {
        for chem_key in &ordered_reactions {
            if chem_to_count.contains_key::<str>(&chem_key) {
                accumulate_reaction_inputs(&mut chem_to_count, output_to_reactions.get(chem_key.as_str()).unwrap());
                break;  // reaction step may have produced other chemicals worth considering for expansion before the others
            }
        }
    }

    *chem_to_count.get("ORE").unwrap()
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> std::result::Result<(), std::boxed::Box<dyn std::error::Error>> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day14/input.txt")[..])),
    };
    let reactions = f.lines().map(|line| reaction_parser::parse(&line.unwrap()).unwrap()).collect::<Vec<Reaction>>();

    let min_ore = calc_min_ore(&reactions, 1);
    println!("Part 1: minimum ORE required to produce 1 FUEL: {}", min_ore);

    // Taking the easy way out!  Guess amounts of fuel and figure out what is
    // the min ore required.  We can manually jump around to get in the
    // neighborhood and then exhaustively search.  Since we figured out the
    // minimal amount of ORE required for 1 FUEL in part 1, we only need to
    // search until we've hit one trillion + part-1-min-ore.
    //
    // Note that this process took about 2 minutes of getting in the neighborhood
    // and then waiting for the search to complete when compiled with cargo
    // release.  The final answer was 2944565 FUEL (using 999999798712 ORE).
    let min_ore_part_1 = min_ore;
    let mut max_fuel_qty = 0;
    for fuel_qty in 2_800_000.. {
        let min_ore = calc_min_ore(&reactions, fuel_qty);
        if min_ore <= 1_000_000_000_000 {
            max_fuel_qty = fuel_qty;
        }
        if min_ore >= 1_000_000_000_000 + min_ore_part_1 {
            println!();
            println!("Part 2: max fuel produced with at most one trillion ({}) ORE: {}", min_ore, max_fuel_qty);
            break;
        }
        if fuel_qty % 1_000 == 0 {
            print!(".");
            std::io::stdout().flush()?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_reaction() {
        let reaction_str = "10 ORE => 10 A";
        assert_eq!(reaction_parser::parse(reaction_str), Ok( Reaction { inputs: vec![Term { qty: 10, chem: "ORE".to_string() }], output: Term { qty: 10, chem: "A".to_string() } }));

        let reaction_str = "7 A, 1 E => 1 FUEL";
        assert_eq!(reaction_parser::parse(reaction_str), Ok( Reaction { inputs: vec![Term { qty: 7, chem: "A".to_string() }, Term { qty: 1, chem: "E".to_string()}], output: Term { qty: 1, chem: "FUEL".to_string() } }));
    }

    fn load_reactions_from_str(lines: &str) -> Vec<Reaction> {
        lines.split("\n").filter_map(|line| {
            let line = line.trim();
            match line {
                ""   => None,
                line => Some(reaction_parser::parse(&line).unwrap()),
            }
        }).collect::<Vec<Reaction>>()
    }

    #[test]
    fn test_load_reactions_from_str() {
        let reaction_str = "10 ORE => 10 A\n7 A, 1 E => 1 FUEL";
        assert_eq!(load_reactions_from_str(reaction_str), vec![
            Reaction { inputs: vec![Term { qty: 10, chem: "ORE".to_string() }], output: Term { qty: 10, chem: "A".to_string() } },
            Reaction { inputs: vec![Term { qty: 7, chem: "A".to_string() }, Term { qty: 1, chem: "E".to_string()}], output: Term { qty: 1, chem: "FUEL".to_string() } },
        ]);
    }

    #[test]
    fn test_load_reactions_from_rust_str() {
        let reaction_str = r"
            10 ORE => 10 A
            7 A, 1 E => 1 FUEL";
        assert_eq!(load_reactions_from_str(reaction_str), vec![
            Reaction { inputs: vec![Term { qty: 10, chem: "ORE".to_string() }], output: Term { qty: 10, chem: "A".to_string() } },
            Reaction { inputs: vec![Term { qty: 7, chem: "A".to_string() }, Term { qty: 1, chem: "E".to_string()}], output: Term { qty: 1, chem: "FUEL".to_string() } },
        ]);
    }

    #[test]
    fn test_accumulate_reaction_inputs() {
        // 5 A => 3 B
        let reaction = Reaction {
            inputs: vec![
                Term { qty: 5, chem: "A".to_string() },
            ],
            output:
                Term { qty: 3, chem: "B".to_string() }
        };

        let mut chem_to_count: HashMap<&str, u64> = HashMap::new();
        chem_to_count.insert("B", 1);  // initialize the backwards reaction search state
        accumulate_reaction_inputs(&mut chem_to_count, &reaction);
        assert_eq!(chem_to_count.get("B"), None);
        assert_eq!(chem_to_count.get("A"), Some(&5));  // To get 1 B, you need 5 A.  You get 1 B plus 2 B extra.

        let mut chem_to_count: HashMap<&str, u64> = HashMap::new();
        chem_to_count.insert("B", 3);  // initialize the backwards reaction search state
        accumulate_reaction_inputs(&mut chem_to_count, &reaction);
        assert_eq!(chem_to_count.get("B"), None);
        assert_eq!(chem_to_count.get("A"), Some(&5));  // To get 3 B, you need 5 A.  You get exactly 3 B.

        let mut chem_to_count: HashMap<&str, u64> = HashMap::new();
        chem_to_count.insert("B", 7);  // initialize the backwards reaction search state
        accumulate_reaction_inputs(&mut chem_to_count, &reaction);
        assert_eq!(chem_to_count.get("B"), None);
        assert_eq!(chem_to_count.get("A"), Some(&15));  // To get 7 B, you need 15 A.  You get 7 B plus 2 B extra.

        let reaction = Reaction {
            inputs: vec![
                Term { qty: 5, chem: "A".to_string() },
                Term { qty: 1, chem: "B".to_string() }
            ],
            output:
                Term { qty: 3, chem: "C".to_string() }
        };
        let mut chem_to_count: HashMap<&str, u64> = HashMap::new();
        chem_to_count.insert("C", 1);  // initialize the backwards reaction search state
        accumulate_reaction_inputs(&mut chem_to_count, &reaction);
        assert_eq!(chem_to_count.get("C"), None);
        assert_eq!(chem_to_count.get("A"), Some(&5));
        assert_eq!(chem_to_count.get("B"), Some(&1));
    }

    #[test]
    fn test_chained_accumulate_reaction_inputs() {
        // 7 A, 1 E => 1 FUEL
        let reaction1 = Reaction {
            inputs: vec![
                Term { qty: 7, chem: "A".to_string() },
                Term { qty: 1, chem: "E".to_string() },
            ],
            output:
                Term { qty: 1, chem: "FUEL".to_string() }
        };

        // 7 A, 1 D => 1 E
        let reaction2 = Reaction {
            inputs: vec![
                Term { qty: 7, chem: "A".to_string() },
                Term { qty: 1, chem: "D".to_string() },
            ],
            output:
                Term { qty: 1, chem: "E".to_string() }
        };

        let mut chem_to_count: HashMap<&str, u64> = HashMap::new();
        chem_to_count.insert("FUEL", 1);  // initialize the backwards reaction search state
        accumulate_reaction_inputs(&mut chem_to_count, &reaction1);
        assert_eq!(chem_to_count.get("FUEL"), None);
        assert_eq!(chem_to_count.get("A"), Some(&7));
        assert_eq!(chem_to_count.get("E"), Some(&1));

        accumulate_reaction_inputs(&mut chem_to_count, &reaction2);
        assert_eq!(chem_to_count.get("FUEL"), None);
        assert_eq!(chem_to_count.get("E"), None);
        assert_eq!(chem_to_count.get("A"), Some(&14));
        assert_eq!(chem_to_count.get("D"), Some(&1));
    }


    #[test]
    fn test_givens_part1a() {
        let reactions_str = r"
            10 ORE => 10 A
            1 ORE => 1 B
            7 A, 1 B => 1 C
            7 A, 1 C => 1 D
            7 A, 1 D => 1 E
            7 A, 1 E => 1 FUEL";
        assert_eq!(calc_min_ore(&load_reactions_from_str(reactions_str), 1), 31);
    }

    #[test]
    fn test_givens_part1b() {
        let reactions_str = r"
            9 ORE => 2 A
            8 ORE => 3 B
            7 ORE => 5 C
            3 A, 4 B => 1 AB
            5 B, 7 C => 1 BC
            4 C, 1 A => 1 CA
            2 AB, 3 BC, 4 CA => 1 FUEL";
        assert_eq!(calc_min_ore(&load_reactions_from_str(reactions_str), 1), 165);
    }

    #[test]
    fn test_givens_part1c() {
        let reactions_str = r"
            157 ORE => 5 NZVS
            165 ORE => 6 DCFZ
            44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
            12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
            179 ORE => 7 PSHF
            177 ORE => 5 HKGWZ
            7 DCFZ, 7 PSHF => 2 XJWVT
            165 ORE => 2 GPVTF
            3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";
        assert_eq!(calc_min_ore(&load_reactions_from_str(reactions_str), 1), 13312);
    }

    #[test]
    fn test_givens_part1d() {
        let reactions_str = r"
            2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
            17 NVRVD, 3 JNWZP => 8 VPVL
            53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
            22 VJHF, 37 MNCFX => 5 FWMGM
            139 ORE => 4 NVRVD
            144 ORE => 7 JNWZP
            5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
            5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
            145 ORE => 6 MNCFX
            1 NVRVD => 8 CXFTF
            1 VJHF, 6 MNCFX => 4 RFSQX
            176 ORE => 6 VJHF";
        assert_eq!(calc_min_ore(&load_reactions_from_str(reactions_str), 1), 180697);
    }

    #[test]
    fn test_givens_part1e() {
        let reactions_str = r"
            171 ORE => 8 CNZTR
            7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
            114 ORE => 4 BHXH
            14 VRPVC => 6 BMBT
            6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
            6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
            15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
            13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
            5 BMBT => 4 WPTQ
            189 ORE => 9 KTJDG
            1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
            12 VRPVC, 27 CNZTR => 2 XDBXC
            15 KTJDG, 12 BHXH => 5 XCVML
            3 BHXH, 2 VRPVC => 7 MZWV
            121 ORE => 7 VRPVC
            7 XCVML => 6 RJRHP
            5 BHXH, 4 VRPVC => 5 LTCX";
        assert_eq!(calc_min_ore(&load_reactions_from_str(reactions_str), 1), 2210736);
    }
}
