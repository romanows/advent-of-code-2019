//! Solution to [Advent of Code 2019 Day 10](https://adventofcode.com/2019/day/10).

use std::cmp;
use std::collections::HashMap;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Helper for parsing data loaded from a file for runtime or from a string
/// literal used in tests.
fn parse_map_yx(lines: impl Iterator<Item = String>) -> Vec<Vec<char>> {
    lines.map(|x| x.trim().chars().collect()).collect()
}

fn print_map_yx(map: &Vec<Vec<char>>) {
    for row in map {
        println!("{}", row.iter().map(|x| x.to_string()).collect::<Vec<String>>().join(""));
    }
}

/// Get an index into a 1-D array that stores data for a 2-D grid.
#[inline]
fn get_index(max_x: usize, x: usize, y: usize) -> usize {
    y * max_x + x
}

#[allow(dead_code)]
fn print_counts(max_x: usize, counts: &Vec<u16>) {
    for cs in counts.chunks(max_x) {
        println!("{:?}", cs);
    }
}

/// Get all possible slopes for a grid that has `max_y` rows and `max_x`
/// columns.  Return is a vec indexed by the x-axis that holds all the valid,
/// minimal y-axis steps.  So if you're looping through all possible slopes, you
/// can loop like:
///
///     for (step_x, step_ys) in slopes.iter().enumerate() {
///         for step_y in step_ys {
///             ...
///         }
///     }
///
/// NOTE: this leaves out the empty slope (0, 0) and the horizontal (1, 0) and
/// vertical (0, 1) slopes since they are generally special case.
fn all_slopes(max_x: usize, max_y: usize) -> Vec<Vec<usize>> {
    let mut slopes: Vec<Vec<usize>> = Vec::new();

    // Flag the points on the grid that we've already considered for slopes
    let mut accounting: Vec<bool> = vec![false; max_x * max_y];

    for x in 0..max_x {
        let mut y_components: Vec<usize> = Vec::new();
        for y in 0..max_y {
            if x == 0 || y == 0 {
                continue;  // Don't include empty, horizontal, or vertical slopes
            }

            let accounting_idx = get_index(max_x, x, y);
            if accounting[accounting_idx] {
                continue;
            }
            accounting[accounting_idx] = true;
            y_components.push(y);

            let mut multiplier = 2;
            loop {
                if multiplier * x >= max_x || multiplier * y >= max_y {
                    break;
                }
                let accounting_idx = multiplier * get_index(max_x, x, y);
                accounting[accounting_idx] = true;
                multiplier += 1;
            }
        }
        slopes.push(y_components);
    }
    slopes
}

fn count_horizontal(map_yx: &Vec<Vec<char>>, max_x: usize, max_y: usize, counts: &mut Vec<u16>) {
    for y in 0..max_y {
        let mut current_x: Option<usize> = None;
        for x in 0..max_x {
            if map_yx[y][x] == '#' {  // is asteroid
                if let Some(cx) = current_x {
                    counts[get_index(max_x, cx, y)] += 1;
                    counts[get_index(max_x, x, y)] += 1;
                }
                current_x = Some(x);
            }
        }
    }
}

fn count_vertical(map_yx: &Vec<Vec<char>>, max_x: usize, max_y: usize, counts: &mut Vec<u16>) {
    for x in 0..max_x {
        let mut current_y: Option<usize> = None;
        for y in 0..max_y {
            if map_yx[y][x] == '#' {  // is asteroid
                if let Some(cy) = current_y {
                    counts[get_index(max_x, x, cy)] += 1;
                    counts[get_index(max_x, x, y)] += 1;
                }
                current_y = Some(y);
            }
        }
    }
}

struct Point {
    pub x: usize,
    pub y: usize,
    pub idx: usize,
}

fn count_rays(map_yx: &Vec<Vec<char>>, max_x: usize, max_y: usize, step_x: usize, step_y: usize, counts: &mut Vec<u16>, visited: &mut Vec<bool>) {
    let signs: [isize; 2] = [-1, 1];
    for sign in signs.iter() {
        let signed_step_x: isize = *sign * step_x as isize;
        visited.iter_mut().map(|x| *x = false).count();  // reset array of visited flags

        // NOTE: if space is relatively sparse wrt asteroids, it makes more sense to
        // scan once and then visit them directly.

        // No point scanning at start_x=0 when our slope will carry us to the left, off
        // the grid.
        let (begin_col, end_col): (usize, usize) = if signed_step_x < 0 {
            (step_x, max_x)
        } else {
            (0, max_x - step_x)
        };

        if begin_col > max_x || end_col > max_x {
            continue;  // Step puts us out of bounds
        }

        let end_row = max_y - step_y;

        if end_row > max_x {
            continue;  // Step puts us out of bounds
        }

        for start_y in 0..end_row {
            for start_x in begin_col..end_col {
                if map_yx[start_y][start_x] == '#' {  // is asteroid
                    let start_idx = get_index(max_x, start_x, start_y);
                    if visited[start_idx] {
                        continue;
                    }
                    visited[start_idx] = true;
                    let mut p: Point = Point { x: start_x, y: start_y, idx: start_idx };  // current point along the ray
                    let mut x: i32 = p.x as i32;
                    let mut y: usize = p.y;
                    loop {
                        x += signed_step_x as i32;
                        if 0 > x || x >= max_x as i32 { break; }

                        y += step_y;
                        if y >= max_y { break; }

                        let ux: usize = x as usize;

                        if map_yx[y][ux] == '#' {  // is asteroid
                            let idx = get_index(max_x, ux, y);
                            visited[idx] = true;
                            counts[p.idx] += 1;
                            counts[idx] += 1;
                            p.x = ux;
                            p.y = y;
                            p.idx = idx;
                        }
                    }
                }
            }
        }
    }
}

fn count_observable_asteroids(map_yx: &Vec<Vec<char>>) -> Vec<u16> {
    // The obvious algorithm is to visit each asteroid and to cast all possible
    // rays from it to see how many intersect at least one asteroid.  This involves
    // visiting every node and casting pretty-much every ray except for when it is
    // clear that all future rays would exit the grid boundaries.
    //
    // One neat thing about this is that it can be parallelized trivially, since
    // the map is immutable and each asteroid's count is independent of each other
    // asteroid's count.
    //
    // An alternative algorithm takes advantage of the fact that our current
    // asteroid is itself visible from other asteroids.  If we cast all possible
    // rays through the grid and count all immediate-pairs of asteroids, then we
    // can effectively count two for each ray we cast.
    //
    // The problem with the alternative algorithm is that for it to be efficient,
    // we need avoid revisiting (for efficiency) and re-counting (for correctness)
    // asteroids that we've already covered in earlier passes.  Probably the
    // simplest way to do this would be to mark off each node we visit for a
    // particular slope and then reset and reuse that memory for the next slope.
    // This means we visit each node once but do not need to visit nodes around
    // already-visited nodes.
    //
    // Really need to compare these two algorithms and optimize a bit, it's kinda
    // interesting and it's not clear that my attempt at being clever really saves
    // anything.

    let (max_x, max_y): (usize, usize) = (map_yx[0].len(), map_yx.len());

    // All possible slopes that we might want to consider.  Slopes collected
    // go from top-left to bottom-right.  One must also consider mirrored slopes
    // from top-right to bottom-left.
    let slopes = all_slopes(max_x, max_y);

    // Accumulates counts of the number of asteroids visible from each other
    // asteroid
    let mut counts: Vec<u16> = vec![0; max_x * max_y];

    // Reusable array marking when we've accounted for an asteroid at (x, y)
    // for a particular slope.
    let mut visited: Vec<bool> = vec![false; max_x * max_y];

    // Handle horizontal and vertical counts in a special case since these
    // don't get "reflected" and we don't need to track which nodes have been
    // visited.
    count_horizontal(&map_yx, max_x, max_y, &mut counts);
    count_vertical(&map_yx, max_x, max_y, &mut counts);

    // For each slope, and for each non-mirrored / mirrored version of it, scan
    // unvisited nodes from right-to-left, top-to-bottom.
    for (step_x, step_ys) in slopes.iter().enumerate() {  // step_x to the right for every step_y down
        for step_y in step_ys {
            count_rays(&map_yx, max_x, max_y, step_x, *step_y, &mut counts, &mut visited);
        }
    }
    counts
}

fn count_max_observable_asteroids(map_yx: &Vec<Vec<char>>) -> u16 {
    let counts = count_observable_asteroids(&map_yx);
    *counts.iter().max().unwrap()
}

#[derive(Debug)]
struct StepVector {
    pub x: usize,
    pub y: usize,
    pub step_x: isize,
    pub step_y: isize,

    /// square of the length; we only need this to sort vectors at the same angle
    pub square_len: u32,
}

fn reduce(p: isize, q: isize) -> (isize, isize) {
    if p == 0 {
        return (0, q / q.abs());
    }
    if q == 0 {
        return (p / p.abs(), 0);
    }
    let mut x = p.abs();
    let mut y = q.abs();
    'outer: loop {
        for d in 2..=cmp::min(x, y) {
            if x % d == 0 && y % d == 0 {
                x /= d;
                y /= d;
                continue 'outer;
            }
        }
        break 'outer;
    }
    (if p < 0 { -x } else { x }, if q < 0 { -y } else { y })
}

fn orientation(step_x: isize, step_y: isize) -> f32 {
    if step_x >= 0 {
        -((step_y as f32).atan2(step_x as f32) - std::f32::consts::FRAC_PI_2)
    } else if step_y >= 0 {
        2.0 * std::f32::consts::PI - ((step_y as f32).atan2(step_x as f32) - std::f32::consts::FRAC_PI_2)
    } else {
        std::f32::consts::FRAC_PI_2 - (step_y as f32).atan2(step_x as f32)
    }
}

impl StepVector {
    fn new(station_x: usize, station_y: usize, x: usize, y: usize) -> Self {
        let step_x: isize = x as isize - station_x as isize;
        let step_y: isize = station_y as isize - y as isize;
        let square_len: u32 = (step_x * step_x + step_y * step_y) as u32;
        let (step_x, step_y) = reduce(step_x, step_y);
        StepVector { x, y, step_x, step_y, square_len }
    }
}

fn obliterate_asteroids_given_station(map_yx: &Vec<Vec<char>>, station_x: usize, station_y: usize) -> Vec<(usize, usize)> {
    // Part 2. Can create vectors whose angle is expressed in minimal step_x, step_y
    // and with an integer length. We scan the map for asteroids and collect all
    // vectors for all asteroids wrt our origin. Then we sort by angle, then by
    // length. Then we just take the next vector for each angle, wrapping around.
    // The 200th vectors will be the one we want.

    // So much effort in Part 1 to try to come up with something efficient and now
    // we just brute-force it :/
    let mut steps_to_vectors: HashMap<(isize, isize), Vec<StepVector>> = HashMap::new();
    for (y, row) in map_yx.iter().enumerate() {
        for (x, c) in row.iter().enumerate() {
            if station_x == x && station_y == y {
                continue;  // Don't count the station itself
            }
            if *c == '#' {
                let step_vector = StepVector::new(station_x, station_y, x, y);
                steps_to_vectors.entry((step_vector.step_x, step_vector.step_y)).or_insert_with(|| Vec::new()).push(step_vector);
            }
        }
    }
    let mut keys: Vec<(isize, isize)> = steps_to_vectors.keys().cloned().collect();
    keys.sort_by(|(a, b), (p, q)| orientation(a.clone(), b.clone()).partial_cmp(&orientation(p.clone(), q.clone())).unwrap());
    let mut asteroids: Vec<(usize, usize)> = Vec::new();
    loop {
        let mut is_empty = true;
        for key in &keys {
            let v: &mut Vec<StepVector> = steps_to_vectors.get_mut(&key).unwrap();
            if ! v.is_empty() {
                is_empty = false;
                v.sort_by_key(|x| std::u32::MAX - x.square_len);  // No need to do this every time, no need to do that when empty
                let a: StepVector = v.pop().unwrap();  // TODO: This is not going to work when we've popped off all asteroids... why does this work?
                asteroids.push((a.x, a.y));
            }
        }
        if is_empty {
            break;
        }
    }
    asteroids
}

fn obliterate_asteroids(map_yx: &Vec<Vec<char>>) -> Vec<(usize, usize)>{
    let counts = count_observable_asteroids(&map_yx);
    let max_count_idx: usize = counts.iter().enumerate().max_by_key(|(_, x)| x.clone()).unwrap().0;
    let max_x = map_yx[0].len();
    let station_y = max_count_idx / max_x;
    let station_x = max_count_idx - (station_y * max_x);
    obliterate_asteroids_given_station(&map_yx, station_x, station_y)
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day10/input.txt")[..])),
    };
    let map_yx: Vec<Vec<char>> = parse_map_yx(f.lines().map(|x| x.unwrap()));
    print_map_yx(&map_yx);

    println!("Part 1 max observable asteroids: {}", count_max_observable_asteroids(&map_yx));

    let asteroids = obliterate_asteroids(&map_yx);
    println!("Part 2 answer: {}", asteroids[199].0 * 100 + asteroids[199].1);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn load_map_yx_from_str(string: &str) -> Vec<Vec<char>> {
        parse_map_yx(string.split("\n").map(|x| x.to_string()))
    }

    #[test]
    fn test_trivial_horizontal() {
        let map = r"##";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx), vec![1, 1]);
    }

    #[test]
    fn test_trivial_vertical() {
        let map = r"#
                    #";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx), vec![1, 1]);
    }

    #[test]
    fn test_trivial_2x2() {
        let map = r"##
                    ##";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx), vec![3, 3, 3, 3]);
    }

    #[test]
    fn test_trivial_3x3() {
        let map = r"###
                    ###
                    ###";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx), vec![5, 7, 5, 7, 8, 7, 5, 7, 5]);
    }

    #[test]
    fn test_given_part1a() {
        let map = r".#..#
                    .....
                    #####
                    ....#
                    ...##";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        let counts = count_observable_asteroids(&map_yx); 
        assert_eq!(counts, vec![0, 7, 0, 0, 7, 0, 0, 0, 0, 0, 6, 7, 7, 7, 5, 0, 0, 0, 0, 7, 0, 0, 0, 8, 7]);
        assert_eq!(counts[get_index(5, 3, 4)], 8);
        assert_eq!(count_max_observable_asteroids(&map_yx), 8);
    }

    #[test]
    fn test_given_part1b() {
        let map = r"......#.#.
                    #..#.#....
                    ..#######.
                    .#.#.###..
                    .#..#.....
                    ..#....#.#
                    #..#....#.
                    .##.#..###
                    ##...#..#.
                    .#....####";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx)[get_index(map_yx[0].len(), 5, 8)], 33);
        assert_eq!(count_max_observable_asteroids(&map_yx), 33);
    }

    #[test]
    fn test_given_part1c() {
        let map = r"#.#...#.#.
                    .###....#.
                    .#....#...
                    ##.#.#.#.#
                    ....#.#.#.
                    .##..###.#
                    ..#...##..
                    ..##....##
                    ......#...
                    .####.###.";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx)[get_index(map_yx[0].len(), 1, 2)], 35);
        assert_eq!(count_max_observable_asteroids(&map_yx), 35);
    }

    #[test]
    fn test_given_part1d() {
        let map = r".#..#..###
                    ####.###.#
                    ....###.#.
                    ..###.##.#
                    ##.##.#.#.
                    ....###..#
                    ..#.#..#.#
                    #..#.#.###
                    .##...##.#
                    .....#.#..";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx)[get_index(map_yx[0].len(), 6, 3)], 41);
        assert_eq!(count_max_observable_asteroids(&map_yx), 41);
    }

    #[test]
    fn test_given_part1e() {
        let map = r".#..##.###...#######
                    ##.############..##.
                    .#.######.########.#
                    .###.#######.####.#.
                    #####.##.#.##.###.##
                    ..#####..#.#########
                    ####################
                    #.####....###.#.#.##
                    ##.#################
                    #####.##.###..####..
                    ..######..##.#######
                    ####.##.####...##..#
                    .#####..#.######.###
                    ##...#.##########...
                    #.##########.#######
                    .####.#.###.###.#.##
                    ....##.##.###..#####
                    .#.#.###########.###
                    #.#.#.#####.####.###
                    ###.##.####.##.#..##";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        assert_eq!(count_observable_asteroids(&map_yx)[get_index(map_yx[0].len(), 11, 13)], 210);
        assert_eq!(count_max_observable_asteroids(&map_yx), 210);
    }

    #[test]
    fn test_reduce() {
        for p in [-7, -5, -3, -2, -1, 1, 2, 3, 5, 7].iter().cloned() {
            for q in [-7, -5, -3, -2, -1, 1, 2, 3, 5, 7].iter().cloned() {
                for m in [1, 2, 3].iter().cloned() {
                    if p < 0 && p == q {
                        assert_eq!(reduce(m * p, m * q), (-1, -1));
                    } else if p > 0 && p == q {
                        assert_eq!(reduce(m * p, m * q), (1, 1));
                    } else if p < 0 && -p == q {
                        assert_eq!(reduce(m * p, m * q), (-1, 1));
                    } else if p > 0 && -p == q {
                        assert_eq!(reduce(m * p, m * q), (1, -1));
                    } else {
                        assert_eq!(reduce(m * p, m * q), (p, q));
                    }
                }
            }
        }

        // Test when one of the components is zero, then we end up with a unit vector
        // after reducing.
        for p in [-7, -5, -3, -2, -1, 1, 2, 3, 5, 7].iter().cloned() {
            for m in [1, 2, 3].iter().cloned() {
                if p < 0 {
                    assert_eq!(reduce(m * p, 0), (-1, 0));
                    assert_eq!(reduce(0, m * p), (0, -1));
                } else {
                    assert_eq!(reduce(m * p, 0), (1, 0));
                    assert_eq!(reduce(0, m * p), (0, 1));
                }
            }
        }
    }

    #[test]
    fn test_orientation() {
        let (n, ne, e, se, s, sw, w, nw) = (
            orientation(0, 1),
            orientation(1, 1),
            orientation(1, 0),
            orientation(1, -1),
            orientation(0, -1),
            orientation(-1, -1),
            orientation(-1, 0),
            orientation(-1, 1)
        );

        // Test main compass directions
        assert_eq!(n, 0.0);
        assert_eq!(ne, std::f32::consts::FRAC_PI_2 * (1.0 / 2.0));
        assert_eq!(e,  std::f32::consts::FRAC_PI_2 * (2.0 / 2.0));
        assert_eq!(se, std::f32::consts::FRAC_PI_2 * (3.0 / 2.0));
        assert_eq!(s,  std::f32::consts::FRAC_PI_2 * (4.0 / 2.0));
        assert_eq!(sw, std::f32::consts::FRAC_PI_2 * (5.0 / 2.0));
        assert_eq!(w,  std::f32::consts::FRAC_PI_2 * (6.0 / 2.0));
        assert_eq!(nw, std::f32::consts::FRAC_PI_2 * (7.0 / 2.0));

        // Test between the main compass directions to make sure each quadrant is
        // increasing in the right way.
        let (nne, nee, see, sse, ssw, sww, nww, nnw) = (
            orientation(1, 2),
            orientation(2, 1),
            orientation(2, -1),
            orientation(1, -2),
            orientation(-1, -2),
            orientation(-2, -1),
            orientation(-2, 1),
            orientation(-1, 2)

        );
        assert!(n  < nne && nne < ne);
        assert!(ne < nee && nee < e);
        assert!(e  < see && see < se);
        assert!(se < sse && sse < s);
        assert!(s  < ssw && ssw < sw);
        assert!(sw < sww && sww < w);
        assert!(w  < nww && nww < nw);
        assert!(nw < nnw && nnw < 2.0 * std::f32::consts::PI);
    }

    #[test]
    fn test_given_part2a() {
        let map = r".#....#####...#..
                    ##...##.#####..##
                    ##...#...#.#####.
                    ..#.....X...###..
                    ..#.#.....#....##";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        let asteroids = obliterate_asteroids_given_station(&map_yx, 8, 3);
        let mut asteroids_ref: Vec<(usize, usize)> = Vec::new();

        // First batch
        asteroids_ref.extend(vec![(8, 1), (9, 0), (9, 1), (10, 0), (9, 2), (11, 1), (12, 1), (11, 2), (15, 1)]);

        // Second batch
        asteroids_ref.extend(vec![(12, 2), (13, 2), (14, 2), (15, 2), (12, 3), (16, 4), (15, 4), (10, 4), (4, 4)]);

        // Third batch
        asteroids_ref.extend(vec![(2, 4), (2, 3), (0, 2), (1, 2), (0, 1), (1, 1), (5, 2), (1, 0), (5, 1)]);

        for (a, b) in asteroids_ref.iter().zip(asteroids.iter()) {
            assert_eq!(a, b);
        }
    }

    #[test]
    fn test_given_part2b() {
        let map = r".#..##.###...#######
                    ##.############..##.
                    .#.######.########.#
                    .###.#######.####.#.
                    #####.##.#.##.###.##
                    ..#####..#.#########
                    ####################
                    #.####....###.#.#.##
                    ##.#################
                    #####.##.###..####..
                    ..######..##.#######
                    ####.##.####...##..#
                    .#####..#.######.###
                    ##...#.##########...
                    #.##########.#######
                    .####.#.###.###.#.##
                    ....##.##.###..#####
                    .#.#.###########.###
                    #.#.#.#####.####.###
                    ###.##.####.##.#..##";
        let map_yx: Vec<Vec<char>> = load_map_yx_from_str(map);
        let asteroids = obliterate_asteroids(&map_yx);
        assert_eq!(asteroids[0], (11, 12));
        assert_eq!(asteroids[1], (12, 1));
        assert_eq!(asteroids[2], (12, 2));
        assert_eq!(asteroids[9], (12, 8));
        assert_eq!(asteroids[19], (16, 0));
        assert_eq!(asteroids[49], (16, 9));
        assert_eq!(asteroids[99], (10, 16));
        assert_eq!(asteroids[198], (9, 6));
        assert_eq!(asteroids[199], (8, 2));
        assert_eq!(asteroids[200], (10, 9));
        assert_eq!(asteroids[298], (11, 1));
    }
}
