//! Solution to [Advent of Code 2019 Day 1](https://adventofcode.com/2019/day/1).

use std::cmp;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Return the amount of fuel needed to lift a given amount of mass when the
/// mass of the fuel itself is ignored.
fn calc_massless_fuel(mass: u32) -> u32 {
    cmp::max(0, (mass as i64 / 3) - 2) as u32
}

/// Return the amount of fuel needed to lift a given amount of mass, taking into
/// account the mass of the fuel.
fn calc_fuel(mass: u32) -> u32 {
    let mut total_fuel = 0;
    let mut last_mass = mass;
    loop {
        let fuel = calc_massless_fuel(last_mass);
        if fuel == 0 {
            break;
        }
        total_fuel += fuel;
        last_mass = fuel;
    }
    total_fuel
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day01/input.txt")[..])),
    };
    let masses: Vec<u32> = f.lines().map(|x| x.unwrap().parse::<u32>().unwrap()).collect();
    let sum_massless_fuel: u32 = masses.iter().copied().map(calc_massless_fuel).sum();
    let sum_fuel: u32 = masses.iter().copied().map(calc_fuel).sum();
    println!("Part 1: Sum of fuel needed for each module when fuel is massless: {}", sum_massless_fuel);
    println!("Part 2: Sum of fuel needed for each module when fuel has mass: {}", sum_fuel);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_given_part1() {
        // For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
        assert_eq!(calc_massless_fuel(12), 2);

        // For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
        assert_eq!(calc_massless_fuel(14), 2);

        // For a mass of 1969, the fuel required is 654.
        assert_eq!(calc_massless_fuel(1969), 654);

        // For a mass of 100756, the fuel required is 33583.
        assert_eq!(calc_massless_fuel(100756), 33583);
    }

    #[test]
    fn test_extra_part1() {
        assert_eq!(calc_massless_fuel(1), 0);
        assert_eq!(calc_massless_fuel(0), 0);
    }

    #[test]
    fn test_given_part2() {
        // A module of mass 14 requires 2 fuel. This fuel requires no further fuel (2 divided by 3 and rounded down is 0, which would call for a negative fuel), so the total fuel required is still just 2.
        assert_eq!(calc_fuel(14), 2);

        // At first, a module of mass 1969 requires 654 fuel. Then, this fuel requires 216 more fuel (654 / 3 - 2). 216 then requires 70 more fuel, which requires 21 fuel, which requires 5 fuel, which requires no further fuel. So, the total fuel required for a module of mass 1969 is 654 + 216 + 70 + 21 + 5 = 966.
        assert_eq!(calc_fuel(1969), 966);

        // The fuel required by a module of mass 100756 and its fuel is: 33583 + 11192 + 3728 + 1240 + 411 + 135 + 43 + 12 + 2 = 50346.
        assert_eq!(calc_fuel(100756), 50346);
    }

    #[test]
    fn test_extra_part2() {
        assert_eq!(calc_fuel(1), 0);
        assert_eq!(calc_fuel(0), 0);
    }
}
