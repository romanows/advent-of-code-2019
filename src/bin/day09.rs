//! Solution to [Advent of Code 2019 Day 9](https://adventofcode.com/2019/day/9).

use advent_of_code_2019::intcode::IntcodeComputer;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day09/input.txt")[..])),
    };
    let intcode_string: String = f.lines().next().unwrap()?;

    let mut intcoder = IntcodeComputer::from_string_state_with_memory(&intcode_string, 4096);
    intcoder.push_input(1);
    intcoder.run();

    println!("Part 1: output value: {}", intcoder.pop_output().unwrap());

    let mut intcoder = IntcodeComputer::from_string_state_with_memory(&intcode_string, 4096);
    intcoder.push_input(2);
    intcoder.run();

    println!("Part 2: output value: {}", intcoder.pop_output().unwrap());

    Ok(())
}
