//! Solution to [Advent of Code 2019 Day 2](https://adventofcode.com/2019/day/2).

// An Intcode program is a list of integers separated by commas (like
// 1,0,0,3,99). To run one, start by looking at the first integer (called
// position 0). Here, you will find an opcode - either 1, 2, or 99. The opcode
// indicates what to do; for example, 99 means that the program is finished and
// should immediately halt. Encountering an unknown opcode means something went
// wrong.
//
// Opcode 1 adds together numbers read from two positions and stores the result
// in a third position. The three integers immediately after the opcode tell
// you these three positions - the first two indicate the positions from which
// you should read the input values, and the third indicates the position at
// which the output should be stored.
//
// For example, if your Intcode computer encounters 1,10,20,30, it should read
// the values at positions 10 and 20, add those values, and then overwrite the
// value at position 30 with their sum.
//
// Opcode 2 works exactly like opcode 1, except it multiplies the two inputs
// instead of adding them. Again, the three integers after the opcode indicate
// where the inputs and outputs are, not their values.
//
// Once you're done processing an opcode, move to the next one by stepping
// forward 4 positions.

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// An Intcode program.
///
/// The terminology "state" is used because the Intcode program can mutate itself.
type IntcodeState = Vec<usize>;

/// Parses a string of intcode into `IntcodeState` integers.
fn parse(s: &str) -> IntcodeState {
    s.split(',').map(|x| x.parse::<usize>().unwrap()).collect()
}

/// Executes the given intcode program, which may mutate itself in-place.
fn run(program: &mut IntcodeState) {
    let mut i = 0;
    loop {
        match program[i] {
            1 => {
                let x = program[program[i + 1]] + program[program[i + 2]];
                let j = program[i + 3];
                program[j] = x;
                i += 4;
            },
            2 => {
                let x = program[program[i + 1]] * program[program[i + 2]];
                let j = program[i + 3];
                program[j] = x;
                i += 4;
            },
            99 => break,
            x => panic!("Unexpected opcode '{}'", x),
        };
    }
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day02/input.txt")[..])),
    };
    let intcode_string: String = f.lines().next().unwrap()?;
    let mut intcode = parse(&intcode_string);

    // Once you have a working computer, the first step is to restore the gravity
    // assist program (your puzzle input) to the "1202 program alarm" state it had
    // just before the last computer caught fire. To do this, before running the
    // program, replace position 1 with the value 12 and replace position 2 with
    // the value 2. What value is left at position 0 after the program halts?
    intcode[1] = 12;
    intcode[2] = 2;

    run(&mut intcode);

    println!("Part 1: value at position 0 after halt: {}", intcode[0]);

    let mut answer_noun = 999;
    let mut answer_verb = 999;
    'outer: for noun in 0..100 {
        for verb in 0..100 {
            let mut intcode = parse(&intcode_string);
            intcode[1] = noun;
            intcode[2] = verb;
            run(&mut intcode);
            if intcode[0] == 19690720 {
                answer_noun = noun;
                answer_verb = verb;
                break 'outer;
            }
        }
    }
    if answer_noun == 999 {
        println!("Part 2: search didn't find a noun, verb pair that satisifies the result condition!");
    } else {
        println!("Part 2: search results in noun={} and verb={} so the form value should be '{}'", answer_noun, answer_verb, answer_noun * 100 + answer_verb);
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_given_part1() {
        assert_eq!(parse("1,0,0,0,99"), vec![1, 0, 0, 0, 99]);

        // 1,0,0,0,99 becomes 2,0,0,0,99 (1 + 1 = 2).
        let mut intcode = parse("1,0,0,0,99");
        run(&mut intcode);
        assert_eq!(intcode, vec![2, 0, 0, 0, 99]);

        // 2,3,0,3,99 becomes 2,3,0,6,99 (3 * 2 = 6).
        let mut intcode = parse("2,3,0,3,99");
        run(&mut intcode);
        assert_eq!(intcode, vec![2, 3, 0, 6, 99]);

        // 2,4,4,5,99,0 becomes 2,4,4,5,99,9801 (99 * 99 = 9801).
        let mut intcode = parse("2,4,4,5,99,0");
        run(&mut intcode);
        assert_eq!(intcode, vec![2, 4, 4, 5, 99, 9801]);

        // 1,1,1,4,99,5,6,0,99 becomes 30,1,1,4,2,5,6,0,99.
        let mut intcode = parse("1,1,1,4,99,5,6,0,99");
        run(&mut intcode);
        assert_eq!(intcode, vec![30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }
}
