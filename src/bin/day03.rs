//! Solution to [Advent of Code 2019 Day 3](https://adventofcode.com/2019/day/3).

// Two wires are connected to a central port and extend outward on a grid.  ...
// You need to find the intersection point closest to the central port.
// Because the wires are on a grid, use the Manhattan distance for this
// measurement. While the wires do technically cross right at the central port
// where they both start, this point does not count, nor does a wire count as
// crossing with itself.
//
// Wires are given LOGO style wrt their common origin like `[UDRL][0-9]+`;
// e.g., U5 would mean that the wire goes up 5 units and U5R3 means that the
// wire goes up 5 and right 3.
//
// Quick and easy solution is to collect the set of points that each wire
// crosses, intersect them, then find the one closest to the origin.
//
// However, some of these paths are very large, given that my input has one
// wire going right ~1000 units and the other wire going left ~1000 units on
// the very first step.
//
// It's quick to calculate the length of wires (modulo overlapping segments),
// so we could figure out the likely shorter wire, collect its points, and then
// iterate through the other wire's points looking for intersections.
//
// We could also quickly calculate maximal bounding boxes and look only at
// points in their intersection.
//
// That gets us to what [clair_resurgent suggested on
// reddit](https://www.reddit.com/r/rust/comments/e4w3hn/hey_rustaceans_got_an_easy_question_ask_here/f9n88v4/),
// which is looking at segment intersections rather than collections of points.
// One segment that's a million points long is still just one segment defined
// by it's origin, direction, and extent.  This is by far the smartest of the
// possible ways to look for intersections.  You can sort one of the
// collections (the one with the fewest segments, probably?) according to it's
// nearest Manhattan distance to the origin even, which might be a good
// heuristic for finding close intersections.  Once you've found one
// intersection, you can cut off any segments that extend beyond that distance
// and discard any segments that are further away.  Neat!
//
// All that said, I don't think we're going to run into time or memory problems
// solving this as a collection of points, and it'll give me a chance to use
// the neat-o functional approach I figured out in my StackOverflow issue about
// chaining iterators: https://stackoverflow.com/a/59187777/867695.
//
// For heuristically faster lookup, we always could bucket the points according
// to one of their coordinates (via hash) and then sort along the second
// coordinate so we could use binary search to quickly find points.  Again, I
// don't think this is really necessary.

use std::collections::HashSet;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};
use std::iter::FromIterator;

/// Point in a cartesian plane.
#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }
}

/// LOGO-like "movement" step used to "draw" wires.
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
struct Step {
    dir: Direction,
    len: u16
}

impl Step {
    fn parse(s: &str) -> Step {
        let dir = match &s[0..1] {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "L" => Direction::Left,
            "R" => Direction::Right,
            e => panic!("Illegal direction '{}'", e),
        };
        let len = s[1..].parse::<u16>().unwrap();
        Step { dir, len }
    }
}

/// Movement direction, used as part of a `Step`.
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
enum Direction {Up, Down, Left, Right}

/// Parses the `wire_path` string and executes its instructions, returning the
/// collection of `Point`s that the wires occupy.
fn get_path_points(wire_path: &str) -> Vec<Point> {
    // Easiest approach wins, let's just collect the sets of points in both paths,
    // intersect them, sort them, and pick the first intersection.  Easier to do
    // this imperatively but I spent a lot of time figuring out a more functional
    // way to do it because I nerd sniped myself, so here goes!

    let wire_steps = wire_path.split(',').map(|x| Step::parse(x));

    fn point_step(p: Point, dir: Direction) -> Point {
        match dir {
            Direction::Up    => Point { x: p.x, y: p.y + 1 },
            Direction::Down  => Point { x: p.x, y: p.y - 1 },
            Direction::Left  => Point { x: p.x - 1, y: p.y },
            Direction::Right => Point { x: p.x + 1, y: p.y },
        }
    }

    // Explodes the individual steps into functions like "U2,R1" becomes:
    //     [|x| point_step(x, Up), |x| point_step(x, Up), |x| point_step(x, Down)]
    let wire_step_fns = wire_steps.flat_map(|step: Step| (0..step.len).map(move |_| move |x: Point| point_step(x, step.dir.clone())) );

    // `fold` lets us chain the steps one after the other.  Note that if we were
    // looking for a subset of all points, we could use this to take less memory
    // than the full set of points.
    let origin: Point = Point::new(0, 0);
    let path_points: Vec<Point> = wire_step_fns.fold(vec![origin], |mut acc, f| {
        acc.push(f(*acc.last().unwrap()));
        acc
    }).split_off(1);  // split_off gets rid of the initial "origin" point at (0, 0)

    path_points
}

/// Returns the smallest Manhattan distance from the origin to the intersection of
/// the two wires (specified as the points they occupy).
fn get_smallest_manhattan_distance_intersection(path1_points: Vec<Point>, path2_points: Vec<Point>) -> u32 {
    let set1_points: HashSet<Point> = HashSet::from_iter(path1_points);
    let set2_points: HashSet<Point> = HashSet::from_iter(path2_points);
    let intersections = set1_points.intersection(&set2_points);
    let mut distances: Vec<u32> = intersections.map(|p: &Point| (p.x.abs() as u32) + (p.y.abs() as u32)).collect();
    distances.sort();
    distances[0]
}

/// Returns the smallest number of steps along both wires from the origin to one of
/// their intersections.
///
/// Similar to `get_smallest_manhattan_distance_intersection`, but considers the
/// distance along the wires.
fn get_fewest_steps_intersection(path1_points: Vec<Point>, path2_points: Vec<Point>) -> u32 {
    // What we should do is figure out how to put references in the HashSet and
    // insure that the HashSet dereferences them rather than using clone(), but I
    // just want to move past this problem.
    let set1_points: HashSet<Point> = HashSet::from_iter(path1_points.clone());
    let set2_points: HashSet<Point> = HashSet::from_iter(path2_points.clone());
    let intersections = set1_points.intersection(&set2_points);
    let mut steps: Vec<u32> = intersections.map(|p: &Point| path1_points.iter().position(|x| x == p).unwrap() as u32 + path2_points.iter().position(|x| x == p).unwrap() as u32).collect();
    steps.sort();

    // Add 2 below because the path points as we've constructed them do not include
    // the starting point, because the origin is not a valid intersection as-per
    // problem statement, so the next point is at position "0" instead of position "1"
    steps[0] + 2
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day03/input.txt")[..])),
    };
    let mut lines = f.lines();
    let wire1_path: String = lines.next().unwrap()?;
    let wire2_path: String = lines.next().unwrap()?;

    let path1_points = get_path_points(&wire1_path);
    let path2_points = get_path_points(&wire2_path);
    let p = get_smallest_manhattan_distance_intersection(path1_points, path2_points);
    println!("Part 1: smallest manhattan distance to intersection point is '{}'", p);

    let path1_points = get_path_points(&wire1_path);
    let path2_points = get_path_points(&wire2_path);
    let p = get_fewest_steps_intersection(path1_points, path2_points);
    println!("Part 2: fewest steps to intersection point is '{}'", p);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_step() {
        assert_eq!(Step::parse("R42"), Step { dir: Direction::Right, len: 42 } );
        assert_eq!(Step::parse("U0"), Step { dir: Direction::Up, len: 0 } );
    }

    #[test]
    fn test_get_path_points() {
        assert_eq!(get_path_points("U0"), vec![]);
        assert_eq!(get_path_points("U1"), vec![Point::new(0, 1)]);
        assert_eq!(get_path_points("U3"), vec![Point::new(0, 1), Point::new(0, 2), Point::new(0, 3)]);
        assert_eq!(get_path_points("D2"), vec![Point::new(0, -1), Point::new(0, -2)]);
        assert_eq!(get_path_points("L2"), vec![Point::new(-1, 0), Point::new(-2, 0)]);
        assert_eq!(get_path_points("R2"), vec![Point::new(1, 0), Point::new(2, 0)]);
        assert_eq!(get_path_points("U2,R2"), vec![Point::new(0, 1), Point::new(0, 2), Point::new(1, 2), Point::new(2, 2)]);
    }

    #[test]
    fn test_given_part1() {
        let path1_points = get_path_points("R75,D30,R83,U83,L12,D49,R71,U7,L72");
        let path2_points = get_path_points("U62,R66,U55,R34,D71,R55,D58,R83");
        assert_eq!(get_smallest_manhattan_distance_intersection(path1_points, path2_points), 159);

        let path1_points = get_path_points("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51");
        let path2_points = get_path_points("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");
        assert_eq!(get_smallest_manhattan_distance_intersection(path1_points, path2_points), 135);
    }

    #[test]
    fn test_given_part2() {
        let path1_points = get_path_points("R75,D30,R83,U83,L12,D49,R71,U7,L72");
        let path2_points = get_path_points("U62,R66,U55,R34,D71,R55,D58,R83");
        assert_eq!(get_fewest_steps_intersection(path1_points, path2_points), 610);

        let path1_points = get_path_points("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51");
        let path2_points = get_path_points("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");
        assert_eq!(get_fewest_steps_intersection(path1_points, path2_points), 410);
    }
}
