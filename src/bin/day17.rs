//! Solution to [Advent of Code 2019 Day 17](https://adventofcode.com/2019/day/17).

use advent_of_code_2019::intcode::{IntcodeComputer};
use itertools;
use std::cmp;
use std::collections::{HashSet, VecDeque};
use std::convert::{TryInto, TryFrom};
use std::env::args_os;
use std::fs::File;
use std::io::{BufReader, BufRead};

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Point {
    x: i16,
    y: i16,
}

impl Point {
    fn new(x: i16, y: i16) -> Point {
        Point { x, y }
    }
}

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn turn(&self, turn: &Turn) -> Direction {
        match turn {
            Turn::Left => match self {
                Direction::North => Direction::West,
                Direction::East => Direction::North,
                Direction::South => Direction::East,
                Direction::West => Direction::South,
            },
            Turn::Right => match self {
                Direction::North => Direction::East,
                Direction::East => Direction::South,
                Direction::South => Direction::West,
                Direction::West => Direction::North,
            },
        }
    }
}

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
pub enum Turn {
    Left,
    Right,
}

impl std::fmt::Display for Turn {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self { Turn::Left => "L", Turn::Right => "R" })
    }
}

fn get_deltas(direction: Direction, turn: Turn) -> (i16, i16) {
    let inverter = match turn {
        Turn::Left => 1,
        Turn::Right => -1,
    };

    let (delta_x, delta_y): (i16, i16) = match direction {
        Direction::North => (inverter * -1, 0),
        Direction::East => (0, inverter * -1),
        Direction::South => (inverter * 1, 0),
        Direction::West => (0, inverter * 1),
    };

    (delta_x, delta_y)
}

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
pub struct Instruction {
    turn: Turn,
    steps: u16,
}

impl Instruction {
    fn new(turn: Turn, steps: u16) -> Instruction {
        Instruction { turn, steps }
    }
}

impl std::fmt::Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{},{}", self.turn, self.steps)
    }
}

fn instructions_to_string(instructions: &[Instruction]) -> String {
    itertools::join(instructions, ",")
}

/// Max string length of the robot main and functions when serialized to their
/// CSV representations.
const MAX_FUNCTION_STRING_LENGTH: usize = 20;

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
struct Robot {
    p: Point,
    d: Direction,
}

impl Robot {
    fn new(x: i16, y: i16, d: Direction) -> Robot {
        Robot { p: Point::new(x, y), d }
    }

    fn run_step(&mut self, instruction: &Instruction) -> Vec<Point> {
        let mut visited: Vec<Point> = Vec::new();

        let (dx, dy) = get_deltas(self.d, instruction.turn);
        self.d = self.d.turn(&instruction.turn);
        for _ in 0..instruction.steps {
            self.p.x += dx;
            self.p.y += dy;
            visited.push(self.p.clone());
        }
        visited
    }

    fn run(&mut self, instructions: &Vec<Instruction>) -> Vec<Point> {
        let mut visited: Vec<Point> = Vec::new();
        for instruction in instructions {
            visited.extend(self.run_step(instruction));
        }
        visited
    }
}

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
struct Program {
    main: Vec<usize>,
    functions: Vec<Vec<Instruction>>,
}

impl Program {
    fn new() -> Program {
        let functions: Vec<Vec<Instruction>> = vec![vec![], vec![], vec![]];
        Program { main: Vec::new(), functions }
    }

    fn main_to_string(&self) -> String {
        itertools::join(self.main.iter().map(|x| match x { 0 => "A", 1 => "B", 2 => "C", _ => panic!("unexpected function index: {}", x) }), ",")
    }

    fn function_to_string(&self, function_idx: usize) -> String {
        itertools::join(self.functions[function_idx].iter(), ",")
    }

    /// Returns `true` if adding any more function calls to main routine would
    /// violate string length limits.
    fn is_main_at_limit(&self) -> bool {
        let len = self.main_to_string().len();
        if len == 0 && len + 1 <= MAX_FUNCTION_STRING_LENGTH {  // One function call doesn't require a comma separator
            false
        } else if len + 2 <= MAX_FUNCTION_STRING_LENGTH {  // Comma then new function call
            false
        } else {
            true
        }
    }

    /// Returns `true` if adding an instruction with the given number of steps would
    /// violate string length limits.
    fn is_function_at_limit(&self, function_idx: usize, steps: usize) -> bool {
        let len = self.function_to_string(function_idx).len();
        let new_instruction_len = 1 + steps.to_string().len();
        if len == 0 && len + new_instruction_len <= MAX_FUNCTION_STRING_LENGTH {  // One instruction call doesn't require a comma separator
            false
        } else if len + new_instruction_len <= MAX_FUNCTION_STRING_LENGTH {  // Comma then new instruction
            false
        } else {
            true
        }
    }
}

impl std::fmt::Display for Program {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let function_portion = itertools::join((0..self.functions.len()).map(|i| self.function_to_string(i)), "\n");
        let is_displaying_video = false;
        write!(f, "{}\n{}\n{}\n", self.main_to_string(), function_portion, if is_displaying_video { "y" } else { "n" })
    }
}

fn parse_map_scaffolding(s: &str) -> Vec<Point> {
    s.trim()
        .split("\n")
        .enumerate()
        .map(|(line_idx, lines)|
            std::iter::repeat(line_idx)
            .zip(lines.trim().chars().enumerate())
            .filter_map(|(line_idx, (col_idx, c))|
                match c {
                    '#' => Some(Point::new(col_idx.try_into().unwrap(), line_idx.try_into().unwrap())),
                    _ => None
                }
            )
        )
        .flatten()
        .collect()
}

fn parse_map_robot(s: &str) -> Robot {
    s.trim()
        .split("\n")
        .enumerate()
        .map(|(line_idx, lines)|
            std::iter::repeat(line_idx)
            .zip(lines.trim().chars().enumerate())
            .filter_map(|(line_idx, (col_idx, c))|
                match c {
                    '^' => Some(Robot::new(col_idx.try_into().unwrap(), line_idx.try_into().unwrap(), Direction::North)),
                    '>' => Some(Robot::new(col_idx.try_into().unwrap(), line_idx.try_into().unwrap(), Direction::East)),
                    'v' => Some(Robot::new(col_idx.try_into().unwrap(), line_idx.try_into().unwrap(), Direction::South)),
                    '<' => Some(Robot::new(col_idx.try_into().unwrap(), line_idx.try_into().unwrap(), Direction::West)),
                    _ => None
                }
            )
        )
        .flatten()
        .next()
        .unwrap()
}

fn get_scaffold_intersections(scaffold_points: &[Point]) -> Vec<Point> {
    // For my map, there are no T-intersections, which if present could be located
    // on the borders of the map.
    let scaffold_points: HashSet<Point> = scaffold_points.iter().cloned().collect();
    let mut scaffold_intersections: Vec<Point> = Vec::new();
    for p in &scaffold_points {
        if scaffold_points.contains(&Point::new(p.x - 1, p.y))
                && scaffold_points.contains(&Point::new(p.x + 1, p.y))
                && scaffold_points.contains(&Point::new(p.x, p.y - 1))
                && scaffold_points.contains(&Point::new(p.x, p.y + 1)) {
            scaffold_intersections.push(p.clone());
        }
    }
    scaffold_intersections
}

fn calc_sum_alignment_params(scaffold_intersections: &Vec<Point>) -> u16 {
    scaffold_intersections.iter().map(|p| u16::try_from(p.x * p.y).unwrap()).sum()
}

/// Given a robot on a scaffolding, what are the possible next one-steps it can
/// take to all intersections or as far along the scaffolding as possible.
fn get_next_steps(scaffold_points: &HashSet<Point>, scaffold_intersections_set: &HashSet<Point>, robot: &Robot) -> Vec<Instruction> {
    let mut next_steps: Vec<Instruction> = Vec::new();
    for turn in &[Turn::Left, Turn::Right] {
        let (dx, dy) = get_deltas(robot.d, *turn);
        let mut current_point = robot.p.clone();
        loop {
            current_point.x += dx;
            current_point.y += dy;

            let get_len = || {
                if dy == 0 {
                    dx * (current_point.x - robot.p.x)
                } else {
                    dy * (current_point.y - robot.p.y)
                }.try_into().unwrap()
            };

            if ! scaffold_points.contains(&current_point) {  // Too far!
                let steps = get_len() - 1;
                if steps > 0 {
                    next_steps.push(Instruction::new(*turn, steps));
                }
                break;
            }

            if scaffold_intersections_set.contains(&current_point) {
                let steps = get_len();  // because the current_step is off the map, we insert an instruction to take us to the current_step where we're still on the map
                if steps > 0 {
                    next_steps.push(Instruction::new(*turn, steps));
                }
            }
        }
    }
    next_steps
}

/// Build a queue of search states.  Visit each and advance each using a BFS
/// strategy to get the next-search-states.  Prune search states that don't
/// cover all points.
///
/// `max_point_overlaps` is the number of allowed point overlaps when following
/// a set of instructions.  For example, going around a simple cycle once will
/// cause the robot to revisit the start of the cycle which counts as one
/// overlap.  If the robot goes around the cycle again, there will be as many
/// overlaps as the number of points in the cycle.  If this exceeds
/// `max_point_overlaps`, then this search state would be discarded.  Users
/// would want the number of overlaps to be at least the number of
/// `scaffold_intersections`.
struct AllPathsIterator<'a> {
    scaffold_points: &'a HashSet<Point>,
    scaffold_intersections: &'a HashSet<Point>,
    robot: &'a Robot,

    /// Heuristic used to discard search states that seem not-promising because they
    /// have a high-degree of self-overlap.  Makes the search much quicker but also
    /// potentially incomplete.
    max_point_overlaps: usize,

    queue: VecDeque<Vec<Instruction>>,
}

impl<'a> AllPathsIterator<'a> {
    fn new(scaffold_points: &'a HashSet<Point>, scaffold_intersections: &'a HashSet<Point>, robot: &'a Robot) -> AllPathsIterator<'a> {
        let mut api = AllPathsIterator { scaffold_points, scaffold_intersections, robot, max_point_overlaps: scaffold_intersections.len() + 1, queue: VecDeque::new() };

        // Frontload the queue
        let next_steps = get_next_steps(scaffold_points, scaffold_intersections, &robot);
        for next_step in next_steps {
            api.queue.push_front(vec![next_step]);
        }

        api
    }
}

impl<'a> Iterator for AllPathsIterator<'a> {
    type Item = Vec<Instruction>;

    fn next(&mut self) -> Option<Vec<Instruction>> {
        let mut path: Option<Vec<Instruction>> = None;
        while let Some(instructions) = self.queue.pop_back() {
            let mut robot_clone = self.robot.clone();
            let visited = robot_clone.run(&instructions);
            let visited_set: HashSet<Point> = visited.iter().cloned().collect();
            if self.scaffold_points.is_subset(&visited_set) {
                path = Some(instructions.clone());
            }
            let num_overlaps = visited.len() - visited_set.len();
            if num_overlaps < self.max_point_overlaps {
                let next_steps = get_next_steps(self.scaffold_points, self.scaffold_intersections, &robot_clone);
                for next_step in next_steps {
                    let mut next_instructions = instructions.clone();
                    next_instructions.push(next_step);
                    self.queue.push_front(next_instructions);
                }
                if path.is_some() {
                    break;
                }
            }
        }
        path
    }
}

fn get_admissable_program(instructions: &[Instruction], program: &mut Program) -> bool {
    let mut is_admissable_program_found = false;

    if instructions.len() == 0 {
        return true;  // last iteration had discovered a legal program
    }

    if program.main_to_string().len() > MAX_FUNCTION_STRING_LENGTH {
        return false;
    }

    // Can we apply an already-defined function?
    let fun_idxs: Vec<usize> = program.functions.iter().enumerate().filter_map(|(i, x)| if x.is_empty() { None } else { Some(i) }).collect();
    for fun_idx in &fun_idxs {
        let fun = &program.functions[*fun_idx];
        if fun.len() > instructions.len() {  // Might be the case that the last function application would go further than the path but still stay on the scaffolding but let's ignore this for now
            continue;
        }
        if fun[..] == instructions[0..fun.len()] {
            program.main.push(*fun_idx);
            is_admissable_program_found = get_admissable_program(&instructions[fun.len()..], program);
            if is_admissable_program_found {
                return true;
            }
            program.main.pop();  // undo adding the function to main, avoiding a clone of the program
        }
    }

    // Can we build a function?
    if let Some(fun_idx) = program.functions.iter().enumerate().filter_map(|(i, x)| if x.is_empty() { Some(i) } else { None }).next() {
        for num_instructions in (1..cmp::min(MAX_FUNCTION_STRING_LENGTH, instructions.len())).rev() {
            let old_function_length = program.functions[fun_idx].len();
            program.functions[fun_idx].extend_from_slice(&instructions[..num_instructions]);
            if program.function_to_string(fun_idx).len() <= MAX_FUNCTION_STRING_LENGTH {
                program.main.push(fun_idx);
                is_admissable_program_found = get_admissable_program(&instructions[num_instructions..], program);
                if is_admissable_program_found {
                    return true;
                }
                program.main.pop();  // undo adding the function to main, avoiding a clone of the program
            }
            program.functions[fun_idx].truncate(old_function_length);
        }
    }

    is_admissable_program_found
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day17/input.txt")[..])),
    };
    let intcode_string: String = f.lines().next().unwrap()?;

    let mut intcoder = IntcodeComputer::from_string_state_with_memory(&intcode_string, 8196);
    intcoder.run();

    fn get_map_from_intcoder_output(intcoder: &mut IntcodeComputer) -> String {
        let mut m: String = String::new();
        while let Some(o) = intcoder.pop_output() {
            m.push(char::from(u8::try_from(o).unwrap_or_else(|_| panic!("couldn't get a u8 from intcode computer output {}", o))));
        }
        m
    }
    let m = get_map_from_intcoder_output(&mut intcoder);
    println!("{}", m);

    let scaffold_points = parse_map_scaffolding(&m);
    let scaffold_intersections = get_scaffold_intersections(&scaffold_points);
    let sum_alignment_param = calc_sum_alignment_params(&scaffold_intersections);
    println!("Part 1, sum of alignment parameters: {}", sum_alignment_param);

    // Part 2
    let robot = parse_map_robot(&m);
    let mut intcoder = IntcodeComputer::from_string_state_with_memory(&intcode_string, 8192);
    intcoder.memory[0] = 2;  // Force the vacuum robot to wake up by changing the value in your ASCII program at address 0 from 1 to 2.

    // Ended up taking a generally inefficient approach because it was efficient
    // enough to solve the problem.  The implementation first searches for paths
    // through the scaffolding from shortest to longest and then attempts to find a
    // legal program to cover the path.
    //
    // This is potentially inefficient because the BFS used to find paths keeps a
    // queue of paths that can grow quite large and take up a lot of memory.  It is
    // probably smarter to combine the path and program search such that there is
    // one search operation carried out by building an always-valid program.
    //
    // Also, we've built a model of the world, meaning how the robot moves on the
    // scaffholding, when we probably could have just outsourced this to the
    // intcode computer.

    let scaffold_points_set: HashSet<Point> = scaffold_points.into_iter().collect();
    let scaffold_intersections_set: HashSet<Point> = scaffold_intersections.into_iter().collect();

    println!("\nPart 2: Searching paths and programs, this may take a few seconds...");
    let mut program = Program::new();
    for instructions in AllPathsIterator::new(&scaffold_points_set, &scaffold_intersections_set, &robot) {
        if get_admissable_program(&instructions, &mut program) {
            break;
        }
    }

    println!("Found a legal program:");
    println!("{}", program);

    format!("{}", program).chars().for_each(|c| intcoder.push_input(u32::try_from(c).unwrap().try_into().unwrap()));
    intcoder.run();

    // I guess the ASCII program will output a map again or something, we only want
    // the final output:
    let mut dust_collected = -1;
    while let Some(x) = intcoder.pop_output() {
        dust_collected = x;
    }
    println!("Part 2: amount of dust collected: {}", dust_collected);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_map_scaffolding() {
        assert_eq!(parse_map_scaffolding("."), vec![]);
        assert_eq!(parse_map_scaffolding("#"), vec![Point::new(0, 0)]);
        assert_eq!(parse_map_scaffolding(".#."), vec![Point::new(1, 0)]);
        assert_eq!(parse_map_scaffolding(".\n#"), vec![Point::new(0, 1)]);
    }

    #[test]
    fn test_get_scaffold_intersections() {
        assert_eq!(get_scaffold_intersections(&[]), vec![]);
        assert_eq!(get_scaffold_intersections(&[Point::new(1, 1)]), vec![]);
        assert_eq!(get_scaffold_intersections(&[Point::new(1, 0), Point::new(0, 1), Point::new(1, 1), Point::new(2, 1), Point::new(1, 2)]), vec![Point::new(1, 1)]);
    }

    #[test]
    fn test_calc_sum_alignment_params() {
        let f = |x, y| Point::new(x, y);
        assert_eq!(calc_sum_alignment_params(&vec![f(2, 2), f(2, 4), f(6, 4), f(10, 4)]), 76);
    }

    #[test]
    fn test_next_steps() {
        // Only one scaffolding and you're on it!
        let robot: Robot = Robot::new(0, 0, Direction::North);
        let scaffold_points: HashSet<Point> = vec![Point::new(0, 0)].into_iter().collect();
        let scaffold_intersections_set: HashSet<Point> = HashSet::new();
        assert_eq!(get_next_steps(&scaffold_points, &scaffold_intersections_set, &robot), Vec::new());

        // Move in one direction one step
        let scaffold_points: HashSet<Point> = vec![Point::new(0, 0), Point::new(1, 0)].into_iter().collect();
        assert_eq!(get_next_steps(&scaffold_points, &scaffold_intersections_set, &robot), vec![Instruction::new(Turn::Right, 1)]);

        // Move in one direction, intersection and an eventual end like: "^#O##"
        let scaffold_points: HashSet<Point> = vec![Point::new(0, 0), Point::new(-1, 0), Point::new(-2, 0)].into_iter().collect();
        assert_eq!(get_next_steps(&scaffold_points, &scaffold_intersections_set, &robot), vec![Instruction::new(Turn::Left, 2)]);

        // Test changing the axis on which we can move
        let robot: Robot = Robot::new(0, 0, Direction::East);
        let scaffold_points: HashSet<Point> = vec![Point::new(0, 0), Point::new(0, 1), Point::new(0, 2)].into_iter().collect();
        assert_eq!(get_next_steps(&scaffold_points, &scaffold_intersections_set, &robot), vec![Instruction::new(Turn::Right, 2)]);

        // Move in all two directions stopping at intersections.  For example,
        // "##O#^#O##" gives you a test case where the instructions are [R2, R4, L2,
        // L4] to reach the intersections and the ends, although the order is
        // implementation dependent and cannot be relied upon.
        let robot: Robot = Robot::new(0, 0, Direction::North);
        let scaffold_points: HashSet<Point> = vec![Point::new(0, 0), Point::new(-1, 0), Point::new(-2, 0), Point::new(-3, 0), Point::new(-4, 0), Point::new(1, 0), Point::new(2, 0), Point::new(3, 0), Point::new(4, 0)].into_iter().collect();
        let scaffold_intersections_set: HashSet<Point> = vec![Point::new(-2, 0), Point::new(2, 0)].into_iter().collect();
        let refs: HashSet<Instruction> = vec![Instruction::new(Turn::Right, 2), Instruction::new(Turn::Right, 4), Instruction::new(Turn::Left, 2), Instruction::new(Turn::Left, 4), ].into_iter().collect();
        let hyps: HashSet<Instruction> = get_next_steps(&scaffold_points, &scaffold_intersections_set, &robot).into_iter().collect();
        assert_eq!(hyps, refs);
    }

    #[test]
    fn test_robot_run() {
        let mut robot: Robot = Robot::new(0, 0, Direction::North);
        let run_points: Vec<Point> = robot.run(&vec![Instruction::new(Turn::Right, 2), Instruction::new(Turn::Right, 3), Instruction::new(Turn::Left, 1)]);
        let f = Point::new;
        assert_eq!(run_points, vec![f(1, 0), f(2, 0), f(2, 1), f(2, 2), f(2, 3), f(3, 3)]);
        assert_eq!(robot, Robot::new(3, 3, Direction::East));
    }

    #[test]
    fn test_given_part1() {
        let m = r"
            ..#..........
            ..#..........
            #######...###
            #.#...#...#.#
            #############
            ..#...#...#..
            ..#####...^..";
        let scaffold_points = parse_map_scaffolding(m);
        let scaffold_intersections = get_scaffold_intersections(&scaffold_points);
        let sum_alignment_param = calc_sum_alignment_params(&scaffold_intersections);
        assert_eq!(sum_alignment_param, 76);
    }

    #[test]
    fn test_given_part2() {
        let m = r"
            #######...#####
            #.....#...#...#
            #.....#...#...#
            ......#...#...#
            ......#...###.#
            ......#.....#.#
            ^########...#.#
            ......#.#...#.#
            ......#########
            ........#...#..
            ....#########..
            ....#...#......
            ....#...#......
            ....#...#......
            ....#####......";
        let scaffold_points = parse_map_scaffolding(m);
        let scaffold_intersections = get_scaffold_intersections(&scaffold_points);
        let robot = parse_map_robot(&m);

        let scaffold_points_set: HashSet<Point> = scaffold_points.into_iter().collect();
        let scaffold_intersections_set: HashSet<Point> = scaffold_intersections.into_iter().collect();

        let mut program = Program::new();
        let mut is_admissable_program = false;
        for instructions in AllPathsIterator::new(&scaffold_points_set, &scaffold_intersections_set, &robot) {
            if get_admissable_program(&instructions, &mut program) {
                is_admissable_program = true;
                break;
            }
        }
        assert!(is_admissable_program);  // not much of a test but there's not much to test here that isn't implementation dependent
    }
}
