//! Solution to [Advent of Code 2019 Day 6](https://adventofcode.com/2019/day/6).

// In part 1, construct a tree and enumerate the number of paths from the root
// node to all other nodes.

extern crate petgraph;

use petgraph::algo;
use petgraph::graphmap::{DiGraphMap, UnGraphMap};
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

fn parse_graph<'a>(graph: &mut DiGraphMap<&'a str, Option<&str>>, edges: &'a Vec<String>) {
    for edge in edges {
        let mut split = edge.split(')');

        let p = graph.add_node(split.next().unwrap());
        let q = graph.add_node(split.next().unwrap());

        graph.add_edge(p, q, None);
    }
}

fn count_direct_indirect(graph: &DiGraphMap<&str, Option<&str>>, n: &str, depth: u32) -> u32 {
    let mut count: u32 = 0;
    for neighbor in graph.neighbors(n) {
        count += count_direct_indirect(graph, neighbor, depth + 1) + depth + 1;
    }
    count
}

fn count_orbital_transfers(graph: &DiGraphMap<&str, Option<&str>>, p: &str, q: &str) -> u32 {
    let ungraph = UnGraphMap::from_edges(graph.all_edges());
    let path = algo::astar(
        &ungraph,
        p,
        |n| n == q,
        |_e: (&str, &str, &Option<&str>)| 1,
        |_| 0,
    );
    let orbital_transfers = path.unwrap().0 - 2;  // Two less because we don't count the endpoints
    orbital_transfers
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day06/input.txt")[..])),
    };
    let lines: Vec<String> = f.lines().map(|x| x.unwrap()).collect();

    let mut graph = DiGraphMap::new();
    parse_graph(&mut graph, &lines);

    println!("Part 1: total number of direct and indirect orbits: {}", count_direct_indirect(&graph, "COM", 0));

    println!("Part 2: minimum number of orbital transfers (number of nodes along the minimal path) between YOU and SAN: {}", count_orbital_transfers(&graph, "YOU", "SAN"));

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use petgraph::Graph;
    use petgraph::graph::NodeIndex;

    #[test]
    fn test_poc() {
        // POC = Proof of Concept

        // Define the tree from the description example.  HOWEVER, since nodes are
        // *moved* into their parents, we can only define this tree from the leaves up
        // which makes it a bit tricky to create from the data we get!  So instead,
        // let's use the petgraph crate.
        //
        // Note that someone online mentioned using adjacency lists, which is what
        // petgraph uses internally.

        #[derive(Debug)]
        struct Node {
            name: String,
            children: Option<Vec<Node>>,
        }

        let f = Node { name: "F".to_string(), children: None };
        let l = Node { name: "L".to_string(), children: None };
        let k = Node { name: "K".to_string(), children: Some(vec![l]) };
        let j = Node { name: "J".to_string(), children: Some(vec![k]) };
        let e = Node { name: "E".to_string(), children: Some(vec![j, f]) };
        let i = Node { name: "I".to_string(), children: None };
        let d = Node { name: "D".to_string(), children: Some(vec![i, e]) };
        let c = Node { name: "C".to_string(), children: Some(vec![d]) };
        let h = Node { name: "H".to_string(), children: None };
        let g = Node { name: "G".to_string(), children: Some(vec![h]) };
        let b = Node { name: "B".to_string(), children: Some(vec![g, c]) };
        let com = Node { name: "COM".to_string(), children: Some(vec![b]) };

        // Recursively print tree structure
        fn print_tree(n: &Node, num_indent: usize) {
            let indent = " ".repeat(num_indent);
            if let Some(children) = &n.children {
                println!("{}{} ->", indent, n.name);
                for child in children {
                    print_tree(&child, num_indent + 2);
                }
            } else {
                println!("{}{}.", indent, n.name);
            }
        }
        print_tree(&com, 0);

        // Count what we're supposed to count
        fn count_direct_indirect(n: &Node, depth: u32) -> u32 {
            let mut count: u32 = 0;
            if let Some(children) = &n.children {
                for child in children {
                    count += count_direct_indirect(child, depth + 1) + depth + 1;
                }
            }
            count
        }
        assert_eq!(count_direct_indirect(&com, 0), 42);
    }

    #[test]
    fn test_poc_petgraph() {
        // POC = Proof of Concept

        let mut graph = Graph::<String, String>::new();  // directed graph
        let com = graph.add_node("COM".to_string());
        let b = graph.add_node("b".to_string());
        let g = graph.add_node("g".to_string());
        let h = graph.add_node("h".to_string());
        let c = graph.add_node("c".to_string());
        let d = graph.add_node("d".to_string());
        let i = graph.add_node("i".to_string());
        let e = graph.add_node("e".to_string());
        let j = graph.add_node("j".to_string());
        let k = graph.add_node("k".to_string());
        let l = graph.add_node("l".to_string());
        let f = graph.add_node("f".to_string());
        graph.extend_with_edges(&[(com, b), (b, g), (g, h), (b, c), (c, d), (d, i), (d, e), (e, j), (j, k), (k, l), (e, f)]);

        fn count_direct_indirect(graph: &Graph<String, String>, n: &NodeIndex, depth: u32) -> u32 {
            let mut count: u32 = 0;
            for neighbor in graph.neighbors(*n) {
                count += count_direct_indirect(graph, &neighbor, depth + 1) + depth + 1;
            }
            count
        }
        assert_eq!(count_direct_indirect(&graph, &com, 0), 42);
    }

    #[test]
    fn test_given_part1() {
        let graph_edges = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L".split('\n').map(|x| x.to_string()).collect();
        let mut graph = DiGraphMap::new();
        parse_graph(&mut graph, &graph_edges);
        assert_eq!(count_direct_indirect(&graph, "COM", 0), 42);
    }

    #[test]
    fn test_given_part2() {
        let graph_edges = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN".split('\n').map(|x| x.to_string()).collect();
        let mut graph = DiGraphMap::new();
        parse_graph(&mut graph, &graph_edges);
        let orbital_transfers = count_orbital_transfers(&graph, "YOU", "SAN");
        assert_eq!(orbital_transfers, 4);
    }
}
