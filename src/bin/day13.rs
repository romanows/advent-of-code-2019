//! Solution to [Advent of Code 2019 Day 13](https://adventofcode.com/2019/day/13).

use advent_of_code_2019::intcode::{IntcodeComputer, IntcodeState};
use std::cmp;
use std::collections::HashMap;
use std::env::args_os;
use std::fmt;
use std::fs::File;
use std::io::{self, BufReader, BufRead, Write};
use std::thread;
use std::time;

pub enum TileType {
    Empty,
    Wall,
    Block,
    Paddle,
    Ball,
}

impl fmt::Display for TileType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            TileType::Empty  => " ",
            TileType::Wall   => "█",
            TileType::Block  => "▓",
            TileType::Paddle => "=",
            TileType::Ball   => "O",
        })
    }
}

struct Tile {
    pub x: usize,
    pub y: usize,
    pub id: TileType,
}

impl Tile {
    pub fn new(x: usize, y: usize, tile_id: usize) -> Self {
        Tile { x, y, id: match tile_id {
            0 => TileType::Empty,
            1 => TileType::Wall,
            2 => TileType::Block,
            3 => TileType::Paddle,
            4 => TileType::Ball,
            _ => TileType::Empty,
        }}
    }
}

fn read_screen_and_score(intcoder: &mut IntcodeComputer, screen: &mut HashMap<(usize, usize), TileType>) -> i64 {
    let mut score: i64 = -1;
    while let Some(x) = intcoder.pop_output() {
        let y = intcoder.pop_output().unwrap();
        let tile_id = intcoder.pop_output().unwrap();
        let tile = Tile::new(x as usize, y as usize, tile_id as usize);
        if x < 0 {
            score = tile_id;
        } else {
            screen.insert((tile.x, tile.y), tile.id);
        }
    }
    score
}

fn print_screen(screen: &HashMap<(usize, usize), TileType>, score: i64) {
    let min_xy = screen.keys().fold((std::usize::MAX, std::usize::MAX), |acc: (usize, usize), x: &(usize, usize)| (cmp::min(acc.0, x.0), cmp::min(acc.1, x.1)));
    let max_xy = screen.keys().fold((std::usize::MIN, std::usize::MIN), |acc: (usize, usize), x: &(usize, usize)| (cmp::max(acc.0, x.0), cmp::max(acc.1, x.1)));
    let mut to_print = String::new();
    to_print.push_str(&format!("{}[2J", 27 as char));  // clears screen
    for y in min_xy.1..=max_xy.1 {
        for x in min_xy.0..=max_xy.0 {
            to_print.push_str(&format!("{}", match screen.get(&(x, y)) { Some(x) => x, _ => &TileType::Empty }));
        }
        to_print.push_str("\n");
    }
    to_print.push_str(&format!("Score: {}", score));
    println!("{}", to_print);
    let _ = io::stdout().flush();
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day13/input.txt")[..])),
    };
    let intcode_string: String = f.lines().next().unwrap()?;

    let mut intcoder = IntcodeComputer::from_string_state_with_memory(&intcode_string, 4096);
    intcoder.run();

    let mut screen: HashMap<(usize, usize), TileType> = HashMap::new();
    read_screen_and_score(&mut intcoder, &mut screen);
    let num_blocks: i32 = screen.values().map(|x| match x { TileType::Block => 1, _ => 0 }).sum();

    println!("Part 1: num blocks: {}", num_blocks);

    print!("Press enter to continue...");
    let _ = io::stdout().flush();
    io::stdin().read_line(&mut String::new()).expect("error: unable to read user input");

    // Part 2
    let mut intcoder = IntcodeComputer::from_string_state_with_memory(&intcode_string, 4096);
    intcoder.memory[0] = 2;  // play for free

    let mut screen: HashMap<(usize, usize), TileType> = HashMap::new();
    let is_user_input = false;
    loop {
        intcoder.run_to_first_input();
        let score = read_screen_and_score(&mut intcoder, &mut screen);
        print_screen(&screen, score);
        thread::sleep(time::Duration::from_millis(5));

        if intcoder.state == IntcodeState::Terminated {
            break;
        }

        let mut input = String::new();
        if is_user_input {
            print!("Your move: ");
            let _ = io::stdout().flush();
            io::stdin().read_line(&mut input).expect("error: unable to read user input");
        } else {
            let (ball, _) = screen.iter().filter(|(_, v)| match v { TileType::Ball => true, _ => false }).next().unwrap();
            let (paddle, _) = screen.iter().filter(|(_, v)| match v { TileType::Paddle => true, _ => false }).next().unwrap();
            input.push_str(match (ball.0).cmp(&paddle.0) {
                cmp::Ordering::Less    => "-1",
                cmp::Ordering::Equal   => "0",
                cmp::Ordering::Greater => "1",
            });
        }
        intcoder.push_input(input.trim().parse::<i64>().unwrap());
    }

    Ok(())
}
