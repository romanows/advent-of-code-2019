//! Solution to [Advent of Code 2019 Day 8](https://adventofcode.com/2019/day/8).

use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day08/input.txt")[..])),
    };
    let image: String = f.lines().next().unwrap()?;

    let (rows, cols): (usize, usize) = (6, 25);
    let layers: Vec<Vec<u8>> = image.chars().map(|x| x.to_digit(10).unwrap() as u8).collect::<Vec<u8>>().chunks(rows * cols).map(|x| x.to_vec()).collect();
    let ans = layers.iter()
        .map(|layer| {
            let counts = layer.iter()
                .fold((0, 0, 0), |(p, q, r), x| match x {
                    0 => (p+1, q, r),
                    1 => (p, q+1, r),
                    2 => (p, q, r+1),
                    _ => (p, q, r)});
             (counts.0, counts.1 * counts.2)
        }).min_by_key(|x| x.0).unwrap().1;

    println!("Part 1: output value: {:?}", ans);

    println!("Part 2: output value:");
    for row in 0..rows {
        for col in 0..cols {
            for layer in &layers {
                let x = layer[row * cols + col];
                if x == 0 {
                    print!(" ");
                    break;
                }
                if x == 1 {
                    print!("#");
                    break;
                }
            }
        }
        println!("");
    }

    Ok(())
}
