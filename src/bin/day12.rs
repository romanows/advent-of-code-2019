//! Solution to [Advent of Code 2019 Day 12](https://adventofcode.com/2019/day/12).

use num::Integer;
use std::collections::HashSet;

#[derive(Debug, Eq, PartialEq, Hash)]
struct Body {
    pub px: i32,
    pub py: i32,
    pub pz: i32,

    pub vx: i32,
    pub vy: i32,
    pub vz: i32,
}

impl Body {
    fn new(px: i32, py: i32, pz: i32) -> Body {
        Body { px, py, pz, vx: 0, vy: 0, vz: 0 }
    }

    fn apply_velocity(&mut self) {
        self.px += self.vx;
        self.py += self.vy;
        self.pz += self.vz;
    }

    fn get_total_energy(&self) -> i32 {
        let potential_energy = self.px.abs() + self.py.abs() + self.pz.abs();
        let kinetic_energy = self.vx.abs() + self.vy.abs() + self.vz.abs();
        potential_energy * kinetic_energy
    }
}

fn apply_pairwise_component_gravity_delta(a: i32, b: i32) -> (i32, i32) {
    if a < b {
        (1, -1)
    } else if a > b {
        (-1, 1)
    } else {
        (0, 0)
    }
}

fn apply_pairwise_gravity(moons: &mut Vec<Body>, i: usize, j: usize) {
    let (dix, djx) = apply_pairwise_component_gravity_delta(moons[i].px, moons[j].px);
    moons.get_mut(i).unwrap().vx += dix;
    moons.get_mut(j).unwrap().vx += djx;

    let (diy, djy) = apply_pairwise_component_gravity_delta(moons[i].py, moons[j].py);
    moons.get_mut(i).unwrap().vy += diy;
    moons.get_mut(j).unwrap().vy += djy;

    let (diz, djz) = apply_pairwise_component_gravity_delta(moons[i].pz, moons[j].pz);
    moons.get_mut(i).unwrap().vz += diz;
    moons.get_mut(j).unwrap().vz += djz;
}

fn step_system(mut moons: &mut Vec<Body>) {
    for i in 0..(moons.len() - 1) {
        for j in i..moons.len() {
            apply_pairwise_gravity(&mut moons, i, j);
        }
    }
    for moon in &mut moons.iter_mut() {
        moon.apply_velocity();
    }
}

fn calc_system_period(mut moons: &mut Vec<Body>) -> u64 {
    // I haven't proven that the first repetition will be the initial position
    // although it would be in a physical system that never lost energy.  However,
    // printing out the first repetition shows that it *is* the initial position
    // and velocity, so we don't have to use hashmaps here to keep track of the
    // original indexes.

    let mut sets: [HashSet<Vec<i32>>; 3] = [HashSet::new(), HashSet::new(), HashSet::new()];
    let mut periods: [Option<usize>; 3] = [None; 3];

    loop {
        for coord in 0..3 {
            let v: Vec<i32> = moons.iter().fold(Vec::new(), |mut acc, b| { match coord {
                    0 => {acc.push(b.px); acc.push(b.vx); acc},
                    1 => {acc.push(b.py); acc.push(b.vy); acc},
                    2 => {acc.push(b.pz); acc.push(b.vz); acc},
                    _ => panic!(),
                }
            });
            if periods[coord].is_none() && sets[coord].contains(&v) {
                // println!("coord {}: {} -- {:?}", coord, sets[coord].len(), v);  // was already present
                periods[coord] = Some(sets[coord].len());
            } else {
                sets[coord].insert(v);
            }
        }
        if periods.iter().all(|a| a.is_some()) {
            break;
        }
        step_system(&mut moons)
    }

    // Need to find the lowest common multiple.
    let (xp, yp, zp) = (periods[0].unwrap() as u64, periods[1].unwrap() as u64, periods[2].unwrap() as u64);
    let mut system_period: u64 = (xp * yp) / xp.gcd(&yp);
    system_period = (system_period * zp) / system_period.gcd(&zp);  // LCD of three numbers is LCD(LCD(a, b), c)
    system_period
}

/// Prints answers to part 1 and 2 to stdout for my input only.
fn main() {
    let mut moons: Vec<Body> = vec![Body::new(15, -2, -6), Body::new(-5, -4, -11), Body::new(0, -6, 0), Body::new(5, 9, 6)];

    (0..1000).for_each(|_| step_system(&mut moons));  // so svelte

    let total_energy: i32 = moons.iter().map(|x| x.get_total_energy()).sum();
    println!("Part 1, total energy: {}", total_energy);

    // To find the cycles, we can take advantage of the fact that all components
    // are independent of one another.  Finding the period of each component
    // separately allows us to find the least-common-multiple which is the period
    // for all three together.

    let mut moons: Vec<Body> = vec![Body::new(15, -2, -6), Body::new(-5, -4, -11), Body::new(0, -6, 0), Body::new(5, 9, 6)];
    let system_period = calc_system_period(&mut moons);
    println!("Part 2, system period: {}", system_period);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_givens_part1() {
        let mut moons: Vec<Body> = vec![Body::new(-1, 0, 2), Body::new(2, -10, -7), Body::new(4, -8, 8), Body::new(3, 5, -1)];

        let to_tuple = |x: &Body| (x.px, x.py, x.pz, x.vx, x.vy, x.vz);

        step_system(&mut moons);  // Step 1
        assert_eq!(to_tuple(&moons[0]), (2, -1, 1, 3, -1, -1));
        assert_eq!(to_tuple(&moons[1]), (3, -7, -4, 1, 3, 3));
        assert_eq!(to_tuple(&moons[2]), (1, -7, 5, -3, 1, -3));
        assert_eq!(to_tuple(&moons[3]), (2, 2, 0, -1, -3, 1));

        step_system(&mut moons);  // Step 2
        assert_eq!(to_tuple(&moons[0]), (5, -3, -1, 3, -2, -2));
        assert_eq!(to_tuple(&moons[1]), (1, -2, 2, -2, 5, 6));
        assert_eq!(to_tuple(&moons[2]), (1, -4, -1, 0, 3, -6));
        assert_eq!(to_tuple(&moons[3]), (1, -4, 2, -1, -6, 2));

        step_system(&mut moons);
        step_system(&mut moons);
        step_system(&mut moons);
        step_system(&mut moons);
        step_system(&mut moons);
        step_system(&mut moons);
        step_system(&mut moons);
        step_system(&mut moons);  // Step 10
        assert_eq!(to_tuple(&moons[0]), (2, 1, -3, -3, -2, 1));
        assert_eq!(to_tuple(&moons[1]), (1, -8, 0, -1, 1, 3));
        assert_eq!(to_tuple(&moons[2]), (3, -6, 1, 3, 2, -3));
        assert_eq!(to_tuple(&moons[3]), (2, 0, 4, 1, -1, -1));
    }

    #[test]
    fn test_givens_part2a() {
        let mut moons: Vec<Body> = vec![Body::new(-1, 0, 2), Body::new(2, -10, -7), Body::new(4, -8, 8), Body::new(3, 5, -1)];
        assert_eq!(calc_system_period(&mut moons), 2772);
    }
}
