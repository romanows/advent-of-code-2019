//! Solution to [Advent of Code 2019 Day 5](https://adventofcode.com/2019/day/5).

use advent_of_code_2019::intcode::IntcodeComputer;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};
use std::iter::repeat_with;

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day05/input.txt")[..] }; }

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(i64, i64)> {
    let intcode_string: String = f.lines().next().unwrap()?;

    let mut intcoder = IntcodeComputer::from_string_state(&intcode_string);
    intcoder.push_input(1);
    intcoder.run();

    // The final output is the one we want; any outputs prior to that should be 0
    let ans_part_1 = repeat_with(|| intcoder.pop_output()).take_while(|x| x.is_some()).last().unwrap().unwrap();

    let mut intcoder = IntcodeComputer::from_string_state(&intcode_string);
    intcoder.push_input(5);
    intcoder.run();
    let ans_part_2 = intcoder.pop_output().unwrap();

    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: output value: {}", ans_part_1);
    println!("Part 2: output value: {}", ans_part_2);
    Ok(())
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        assert_eq!(main_quiet(f).unwrap(), (5821753, 11956381));
    }
}
