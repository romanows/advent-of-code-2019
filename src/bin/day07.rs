//! Solution to [Advent of Code 2019 Day 7](https://adventofcode.com/2019/day/7).

extern crate permutohedron;

use advent_of_code_2019::intcode::{IntcodeComputer, IntcodeState};
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Compile-time inclusion of my input data for this problem.
///
/// Defined like this so we can use it in `main` as well as the `regression` test
/// and defined as a macro because `include_bytes` requires a string literal as an
/// argument and const/static filename strings won't work.
macro_rules! include_my_input_bytes { () => { &include_bytes!("../../data/day07/input.txt")[..] }; }

fn run_amplifier_chain(program: &str, phases: &[i64]) -> i64 {
     let mut next_input = 0;
     for phase in phases {
         let mut intcoder = IntcodeComputer::from_string_state(program);
         intcoder.push_input(*phase);
         intcoder.push_input(next_input);
         intcoder.run();
         next_input = intcoder.pop_output().unwrap();
     }
     next_input
}

fn find_max_system_output(program: &str, system_fn: &dyn Fn(&str, &[i64]) -> i64, start_phase: i64, end_phase: i64) -> i64 {
    let mut phase_settings: Vec<i64> = (start_phase..end_phase).collect();
    let phase_heap = permutohedron::Heap::new(&mut phase_settings);
    let max_output = phase_heap.map(|x| system_fn(program, &x)).max().unwrap();
    max_output
}

fn run_amplifier_loop(program: &str, phases: &[i64]) -> i64 {
     let mut intcoders: Vec<IntcodeComputer> = phases.iter().map(|phase| (phase, IntcodeComputer::from_string_state(program))).map(|(phase, mut intcoder)| {intcoder.push_input(*phase); intcoder}).collect();

     let mut next_input = 0;
     'outer: while ! intcoders.iter().any(|x| x.state == IntcodeState::Terminated) {
         for intcoder in &mut intcoders {
             intcoder.push_input(next_input);
             intcoder.run_to_first_output();
             if intcoder.state == IntcodeState::Terminated {
                 break 'outer;
             }
             next_input = intcoder.pop_output().unwrap();
         }
     }
     next_input
}

/// Helper for regression testing; calculates and returns the answers to parts 1 and 2.
fn main_quiet(f: Box<dyn BufRead>) -> io::Result<(i64, i64)> {
    let intcode_string: String = f.lines().next().unwrap()?;
    let ans_part_1 = find_max_system_output(&intcode_string, &run_amplifier_chain, 0, 5);
    let ans_part_2 = find_max_system_output(&intcode_string, &run_amplifier_loop, 5, 10);
    Ok((ans_part_1, ans_part_2))
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(include_my_input_bytes!())),
    };
    let (ans_part_1, ans_part_2) = main_quiet(f)?;
    println!("Part 1: output value: {}", ans_part_1);
    println!("Part 2: output value: {}", ans_part_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_given_part1() {
        let program = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0";
        assert_eq!(run_amplifier_chain(program, &[4, 3, 2, 1, 0]), 43210);
        assert_eq!(find_max_system_output(program, &run_amplifier_chain, 0, 5), 43210);

        let program = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0";
        assert_eq!(run_amplifier_chain(program, &[0, 1, 2, 3, 4]), 54321);
        assert_eq!(find_max_system_output(program, &run_amplifier_chain, 0, 5), 54321);

        let program = "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0";
        assert_eq!(run_amplifier_chain(program, &[1, 0, 4, 3, 2]), 65210);
        assert_eq!(find_max_system_output(program, &run_amplifier_chain, 0, 5), 65210);
    }

    #[test]
    fn test_given_part2() {
        let program = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5";
        assert_eq!(run_amplifier_loop(program, &[9, 8, 7, 6, 5]), 139629729);
        assert_eq!(find_max_system_output(program, &run_amplifier_loop, 5, 10), 139629729);

        let program = "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10";
        assert_eq!(run_amplifier_loop(program, &[9, 7, 8, 5, 6]), 18216);
        assert_eq!(find_max_system_output(program, &run_amplifier_loop, 5, 10), 18216);
    }

    #[test]
    fn regression() {
        let f = Box::new(BufReader::new(include_my_input_bytes!()));
        assert_eq!(main_quiet(f).unwrap(), (87138, 17279674));
    }
}
