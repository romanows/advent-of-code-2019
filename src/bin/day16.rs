//! Solution to [Advent of Code 2019 Day 16](https://adventofcode.com/2019/day/16).

use std::convert::TryInto;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

/// Converts a string of digits to a Vec of ints.
fn to_vec(input: &str) -> Vec<i64> {
    input.chars().map(|x| i64::from(x.to_digit(10).unwrap())).collect()
}

/// Get an exploded base filter.  For an index of 1, the base filter `[0, 1, 0, -1]`
/// is exploded to `[0, 1, 1, 0, 0, -1, -1]`.
fn get_exploded_base_filter(len: usize, idx: usize) -> Vec<i64> {
    let mut filter: Vec<i64> = Vec::with_capacity(len);
    let base = [0, 1, 0, -1];
    let mut base_idx = 0;
    let mut element_count = idx;
    while filter.len() < len {
        if element_count == 0 {
            base_idx = (base_idx + 1) % base.len();
            element_count = idx + 1;
        }
        filter.push(base[base_idx]);
        element_count -= 1;
    }
    filter
}

/// Calculates one output index `idx` in the FFT calculation.
fn multiply_filter(input: &Vec<i64>, idx: usize) -> i64 {
    input.iter().zip(get_exploded_base_filter(input.len(), idx)).map(|(x, y)| x * y).sum::<i64>().abs() % 10
}

/// Calculate `ceil(x / y)`.
fn div_ceil<P: TryInto<u64>, Q: TryInto<u64>>(x: P, y: Q) -> u64 {
    let x: u64 = x.try_into().ok().unwrap();
    let y: u64 = y.try_into().ok().unwrap();
    let q: u64 = x / y;
    if x % y != 0 {
        q + 1
    } else {
        q
    }
}

/// Calculates one output index `idx` in the FFT calculation.  More efficient
/// than `multiply_filter`.  However, turns out that the `repetitions` isn't
/// useful since it essentially only applies to the first phase of a multi-phase
/// FFT, after which `repetitions = 1` for the remaining phases.
fn multiply_filter_2(input: &Vec<i64>, idx: usize, repetitions: u64) -> i64 {
    // This approach treats the base filter `[0, 1, 0, -1]` as "selecting" elements
    // from the original array to be summed and subtracted (so no trivial
    // multiplications with the filter elements) and doesn't require the filter to
    // be explicitly stored nor the input to be explicitly repeated as part 2
    // requires.
    let len: usize = input.len();
    let repetitions: usize = repetitions.try_into().unwrap();

    // Number of exploded, repeated base filters.  Note that the last exploded
    // filter repetition may be truncated depending on the length of the repeated
    // input sequence.
    let base_filter_length = 4;
    let max_exploded_base_filter_repetitions: usize = div_ceil(len * repetitions, (idx + 1) * base_filter_length).try_into().unwrap();

    // A rough example to illustrate how the indexing calculation works.  For
    // output index 2 (idx=2), we explode and repeat the base filter such that it
    // looks like the following:
    //
    // 0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, 0, 0, 1, 1, 1, 0, 0, 0, ...
    //                                              ^ start of second repetition of exploded base filter selection
    //             ^ end of first repetition of exploded base filter selection
    //       ^ start of first repetition of exploded base filter selection
    //
    // So the math is to simply calculate the offset to a particular exploded base
    // filter selection and then to iterate through the selected elements that
    // follow.  Since the `-1` selected elements are also at a know offset, we can
    // account for them at the same time.  After we're through with the current the
    // exploded base filter, we move onto the next.

    let mut selected: i64 = 0;
    for exploded_base_filter_idx in 0..max_exploded_base_filter_repetitions {
        let pos_idx: usize = (idx + exploded_base_filter_idx * (idx + 1) * 4).try_into().unwrap();
        for exploded_base_filter_selection_idx in 0..(idx + 1) {
            let pos_idx: usize = pos_idx + exploded_base_filter_selection_idx;
            let neg_idx: usize = pos_idx + (idx + 1) * 2;

            // We make sure that the beginning of the chunk is going to be present in the
            // input array but the end index of a run in the chunk may not be, so we'll
            // test for index-in-bounds here.
            if pos_idx < len * repetitions {
                selected += input[pos_idx % usize::from(len)];
                if neg_idx < len * repetitions {
                    selected -= input[neg_idx % usize::from(len)];
                }
            }
        }
    }
    selected.abs() % 10  // mod 10 gets the ones digit
}

/// Performs one phase of the FFT, using `multiply_filter`.
fn step_phase(input: &Vec<i64>) -> Vec<i64> {
    let mut output: Vec<i64> = Vec::new();
    for i in 0..input.len() {
        output.push(multiply_filter(&input, i));
    }
    output
}

/// Performs one phase of the FFT, using `multiply_filter_2`.
fn step_phase_2(input: &Vec<i64>, repetitions: u64) -> Vec<i64> {
    let mut output: Vec<i64> = Vec::new();
    for i in 0..(input.len() * repetitions as usize) {
        output.push(multiply_filter_2(&input, i, repetitions));
    }
    output
}

/// Performs the full `n` phase FFT, using `multiply_filter`.
fn fft(input: &Vec<i64>, n: usize) -> Vec<i64> {
    println!("iter 0");
    let mut output = step_phase(&input);
    for i in 1..n {
        println!("iter {}", i);
        output = step_phase(&output);
    }
    output
}

/// Performs the full `n` phase FFT, using `multiply_filter_2`.
fn fft_2(input: &Vec<i64>, n: usize, repetitions: u64) -> Vec<i64> {
    println!("iter 0");
    let mut output = step_phase_2(&input, repetitions);
    for i in 1..n {
        println!("iter {}", i);
        output = step_phase_2(&output, 1);
    }
    output
}

/// Special case solution.  Phase can be calculated trivially when the index is
/// in the latter half of the array since the filter effectively "selects" all
/// elements until the end of the array.
fn step_phase_hack(input: &Vec<i64>, start_idx: usize) -> Vec<i64> {
    if start_idx * 2 < input.len() {
        panic!("Hack only works for large message offset indexes into the input; message_offset {} into Vec of length {}", start_idx, input.len());
    }
    let mut output: Vec<i64> = vec![0; start_idx];
    let mut selected = input[start_idx..].iter().sum::<i64>();
    output.push(selected % 10);
    for i in (start_idx + 1)..(input.len()) {
        selected -= input[i-1];
        output.push(selected % 10);
    }
    output
}

/// Special case solution when the start_idx is in the latter half of the array.
fn fft_hack(input: &Vec<i64>, n: usize, start_idx: usize) -> Vec<i64> {
    if start_idx * 2 < input.len() {
        panic!("Hack only works for large message offset indexes into the input; message_offset {} into Vec of length {}", start_idx, input.len());
    }
    let mut output: Vec<i64> = step_phase_hack(&input, start_idx);
    for i in 1..n {
        println!("iter {}, {}", i, output.len());
        output = step_phase_hack(&output, start_idx);
    }
    output
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day16/input.txt")[..])),
    };
    let input_txt = f.lines().next().unwrap()?;
    let input = to_vec(&input_txt);
    let output = fft(&input, 100);
    println!("Part 1: first 8 digits: {}", output[..8].iter().map(|x| x.to_string()).collect::<Vec<String>>().join(""));

    // Part 2.  I ended up implementing a special case solution that works for the
    // problem input.  After having gotten the problem correct for my input, I went
    // online and found that there are extensions of this that end up working okay
    // for the general case.
    //
    // The trick is seen in `step_phase_hack` and relies on the filter "selecting"
    // a very long run of input elements for the filtering calculation.  Finding a
    // way to efficiently calculate the output elements we need when the input is
    // repeated 10_000 times is the crux of the matter.
    let message_offset: usize = input[..7].iter().map(|x| x.to_string()).collect::<Vec<String>>().join("").parse::<usize>().unwrap();

    let num_copies = 10_000;
    let mut extended_input = Vec::with_capacity(input.len() * num_copies);
    for _ in 0..num_copies {
        extended_input.extend_from_slice(&input);
    }
    let output = fft_hack(&extended_input, 100, message_offset);
    println!("Part 2, message: {}", output[message_offset..(message_offset + 8)].iter().map(|x| x.to_string()).collect::<Vec<String>>().join(""));

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_vec() {
        assert_eq!(to_vec("1234"), vec![1, 2, 3, 4i64]);
    }

    #[test]
    fn test_get_exploded_base_filter() {
        assert_eq!(get_exploded_base_filter(8, 0), vec![1, 0, -1, 0, 1,  0, -1, 0]);
        assert_eq!(get_exploded_base_filter(8, 1), vec![0, 1,  1, 0, 0, -1, -1, 0]);
        assert_eq!(get_exploded_base_filter(8, 2), vec![0, 0,  1, 1, 1,  0,  0, 0]);
        assert_eq!(get_exploded_base_filter(8, 7), vec![0, 0,  0, 0, 0,  0,  0, 1]);
    }

    #[test]
    fn test_multiply_filter() {
        let input: Vec<i64> = vec![1, 2, 3, 4, 5, 6, 7, 8];
        assert_eq!(multiply_filter(&input, 0), 4);
        assert_eq!(multiply_filter(&input, 1), 8);
        assert_eq!(multiply_filter(&input, 2), 2);
        assert_eq!(multiply_filter(&input, 3), 2);
        assert_eq!(multiply_filter(&input, 4), 6);
        assert_eq!(multiply_filter(&input, 5), 1);
        assert_eq!(multiply_filter(&input, 6), 5);
        assert_eq!(multiply_filter(&input, 7), 8);
    }

    #[test]
    fn test_multiply_filter_2_no_repeat() {
        let input: Vec<i64> = vec![1, 2, 3, 4, 5, 6, 7, 8];
        for i in 0..8 {
            assert_eq!(multiply_filter_2(&input, i, 1), multiply_filter(&input, i), "no input repetition, index {}", i);
        }
    }

    #[test]
    fn test_multiply_filter_2_repeat_1() {
        let input: Vec<i64> = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let num_copies = 2;
        let mut extended_input = Vec::with_capacity(input.len() * num_copies);
        for _ in 0..num_copies {
            extended_input.extend_from_slice(&input);
        }
        for i in 0..8 {
            assert_eq!(multiply_filter_2(&input, i, num_copies as u64), multiply_filter(&extended_input, i), "input repetition {}, index {}", num_copies, i);
        }
    }

    #[test]
    fn test_multiply_filter_2_repeat_36() {
        let input: Vec<i64> = vec![1, 2, 3, 4, 5, 6, 7, 8];
        let num_copies = 37;
        let mut extended_input = Vec::with_capacity(input.len() * num_copies);
        for _ in 0..num_copies {
            extended_input.extend_from_slice(&input);
        }
        for i in 0..8 {
            assert_eq!(multiply_filter_2(&input, i, num_copies as u64), multiply_filter(&extended_input, i), "input repetition {}, index {}", num_copies, i);
        }
    }

    #[test]
    fn test_multiply_filter_2_givens_part2a() {
        let input = to_vec("03036732577212944063491565474664");
        let num_copies = 37;
        let mut extended_input = Vec::with_capacity(input.len() * num_copies);
        for _ in 0..num_copies {
            extended_input.extend_from_slice(&input);
        }
        for i in 1..2 {
            assert_eq!(multiply_filter_2(&input, i, num_copies.try_into().unwrap()), multiply_filter(&extended_input, i), "input repetition {}, index {}", num_copies, i);
        }
    }

    #[test]
    fn test_div_ceil_simple() {
        assert_eq!(div_ceil(3, 2), 2);
        assert_eq!(div_ceil(3, 3), 1);
        assert_eq!(div_ceil(3, 4), 1);
    }

    #[test]
    fn test_div_ceil_max() {
        assert_eq!(div_ceil(std::u64::MAX, std::u64::MAX), 1);
    }

    #[test]
    fn test_div_ceil_types() {
        assert_eq!(div_ceil(3usize, 2i8), 2);
    }

    #[test]
    fn test_givens_part1a() {
        let input = to_vec("12345678");
        assert_eq!(step_phase(&input), to_vec("48226158"));

        assert_eq!(fft(&input, 1), to_vec("48226158"));
        assert_eq!(fft(&input, 2), to_vec("34040438"));
        assert_eq!(fft(&input, 3), to_vec("03415518"));
        assert_eq!(fft(&input, 4), to_vec("01029498"));
    }

    #[test]
    fn test_givens_part1a_fft_2() {
        let input = to_vec("12345678");
        assert_eq!(step_phase_2(&input, 1), to_vec("48226158"));

        assert_eq!(fft_2(&input, 1, 1), fft(&input, 1));
    }

    #[test]
    fn test_givens_part1b() {
        let input = to_vec("80871224585914546619083218645595");
        assert_eq!(fft(&input, 100)[..8], to_vec("24176176")[..]);
    }

    #[test]
    fn test_givens_part1b_fft_2() {
        let input = to_vec("80871224585914546619083218645595");
        assert_eq!(fft_2(&input, 100, 1), fft(&input, 100));
    }

    #[test]
    fn test_givens_part1c() {
        let input = to_vec("19617804207202209144916044189917");
        assert_eq!(fft(&input, 100)[..8], to_vec("73745418")[..]);
    }

    #[test]
    fn test_givens_part1c_fft_2() {
        let input = to_vec("19617804207202209144916044189917");
        assert_eq!(fft_2(&input, 100, 1), fft(&input, 100));
    }

    #[test]
    fn test_givens_part1d() {
        let input = to_vec("69317163492948606335995924319873");
        assert_eq!(fft(&input, 100)[..8], to_vec("52432133")[..]);
    }

    #[test]
    fn test_givens_part1d_fft_2() {
        let input = to_vec("69317163492948606335995924319873");
        assert_eq!(fft_2(&input, 100, 1), fft(&input, 100));

        let num_copies = 11;
        let mut extended_input = Vec::with_capacity(input.len() * num_copies);
        for _ in 0..num_copies {
            extended_input.extend_from_slice(&input);
        }
        assert_eq!(fft_2(&input, 100, num_copies.try_into().unwrap()), fft(&extended_input, 100), "testing with repetitions");
    }

    #[test]
    fn test_givens_part2a() {
        let input = to_vec("03036732577212944063491565474664");
        let message_offset = input[..7].iter().map(|x| x.to_string()).collect::<Vec<String>>().join("").parse::<usize>().unwrap();
        let num_copies = 10_000;
        let mut extended_input = Vec::with_capacity(input.len() * num_copies);
        for _ in 0..num_copies {
            extended_input.extend_from_slice(&input);
        }
        let output = fft_hack(&extended_input, 100, message_offset);
        assert_eq!(output[message_offset..(message_offset + 8)], to_vec("84462026")[..]);
    }

    #[test]
    fn test_givens_part2b() {
        let input = to_vec("02935109699940807407585447034323");
        let message_offset = input[..7].iter().map(|x| x.to_string()).collect::<Vec<String>>().join("").parse::<usize>().unwrap();
        let num_copies = 10_000;
        let mut extended_input = Vec::with_capacity(input.len() * num_copies);
        for _ in 0..num_copies {
            extended_input.extend_from_slice(&input);
        }
        let output = fft_hack(&extended_input, 100, message_offset);
        assert_eq!(output[message_offset..(message_offset + 8)], to_vec("78725270")[..]);
    }

    #[test]
    fn test_givens_part2c() {
        let input = to_vec("03081770884921959731165446850517");
        let message_offset = input[..7].iter().map(|x| x.to_string()).collect::<Vec<String>>().join("").parse::<usize>().unwrap();
        let num_copies = 10_000;
        let mut extended_input = Vec::with_capacity(input.len() * num_copies);
        for _ in 0..num_copies {
            extended_input.extend_from_slice(&input);
        }
        let output = fft_hack(&extended_input, 100, message_offset);
        assert_eq!(output[message_offset..(message_offset + 8)], to_vec("53553731")[..]);
    }
}
