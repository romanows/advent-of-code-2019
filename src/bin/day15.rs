//! Solution to [Advent of Code 2019 Day 15](https://adventofcode.com/2019/day/15).

use advent_of_code_2019::intcode::{IntcodeComputer};
use std::collections::HashSet;
use std::collections::VecDeque;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

#[derive(Debug, Copy, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn to_intcode_input(&self) -> i64 {
        match self {
            Direction::North => 1,
            Direction::East  => 4,
            Direction::South => 2,
            Direction::West  => 3,
        }
    }
}

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Point {
    x: i16,
    y: i16,
}

impl Point {
    fn new(x: i16, y: i16) -> Point {
        Point { x, y }
    }

    fn step(&self, d: Direction) -> Point {
        match d {
            Direction::North => Point { x: self.x, y: self.y + 1 },
            Direction::East  => Point { x: self.x + 1, y: self.y },
            Direction::South => Point { x: self.x, y: self.y - 1 },
            Direction::West  => Point { x: self.x - 1, y: self.y },
        }
    }
}

struct SearchState {
    intcoder: IntcodeComputer,
    point: Point,
    num_steps: u16,
    is_goal: bool,
}

impl SearchState {
    pub fn next(&self, visited: &HashSet<Point>, d: Direction) -> Option<SearchState> {
        // Where would we get with our next movement and have we already visited it?
        let next_point = self.point.step(d);
        if visited.contains(&next_point) {
            None
        } else {
            let mut next_intcoder = self.intcoder.clone();
            next_intcoder.push_input(d.to_intcode_input());
            next_intcoder.run_to_first_output();
            let outcome = next_intcoder.pop_output().expect("unexpected lack of output");
            next_intcoder.run_to_first_input();
            match outcome {
                0 => None, // drone hit a wall, can't go to the next_point, so we don't return a next state
                1 => Some(SearchState { intcoder: next_intcoder, point: next_point, num_steps: self.num_steps + 1, is_goal: false}),
                2 => Some(SearchState { intcoder: next_intcoder, point: next_point, num_steps: self.num_steps + 1, is_goal: true}),
                e => panic!("Unexpected intcode output: {}", e),
            }
        }
    }
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day15/input.txt")[..])),
    };
    let intcode_string: String = f.lines().next().unwrap()?;

    // Part 1 -- What is the fewest number of movement commands required to move
    // the repair droid from its starting position to the location of the oxygen
    // system?
    //
    // One way to do this is to explore the space around the origin at increasingly
    // large step-distances until we encounter the oxygen system.  That should let
    // us build a graph of the map connectivity and find the shortest path to the
    // oxygen sensor.  It may be less than straightforward if the map is a maze,
    // which is why we still might want to use the graph approach.
    //
    // So, for each increasing distance, we can add a bunch of points to explore
    // onto the queue.  We can pop a point, calculate the shortest distance to that
    // point through the map we know of and go there, or put it on a leftovers
    // queue if we can't get there.  Once the robot makes it to that point, we can
    // explore in all directions (nssnweew) to determine what other points are
    // accessible and add edges in our graph, accordingly.  Once we find the oxygen
    // station, we exit the exploration phase and just find the shortest path,
    // returning the number of edges in that path as our answer to part 1.
    //
    // Could also do a BFS by cloning the intcode state for every new step and
    // putting those into a queue (along with the number of steps taken thus far).
    // Then when you pop a state, you take another step and increment the number of
    // steps.  Proceeding in this fashion will find the min number of steps to the
    // oxygen station.  It'll cost a lot more memory, but we're pretty memory
    // efficient already.

    let mut search_states: VecDeque<SearchState> = VecDeque::new();

    let intcoder = IntcodeComputer::from_string_state_with_memory(&intcode_string, 2048);
    let point = Point::new(0, 0);

    search_states.push_front(SearchState { intcoder, point, num_steps: 0, is_goal: false });

    let mut visited: HashSet<Point> = HashSet::new();
    visited.insert(point);

    let oxygen_system_state: Option<SearchState>;

    'outer: loop {
        let state = search_states.pop_back().unwrap();

        for d in &[Direction::North, Direction::East, Direction::South, Direction::West ] {
            match state.next(&visited, *d) {
                Some(next_state) => if next_state.is_goal {
                    println!("Part 1: found oxygen system after {} steps", next_state.num_steps);
                    oxygen_system_state = Some(next_state);
                    break 'outer;
                } else {
                    visited.insert(next_state.point.clone());
                    search_states.push_front(next_state);
                },
                None => (),
            }
        }
    }

    // Part 2 -- How many minutes will it take to fill with oxygen?
    //
    // Can solve this with a variant of the BFS we used before.  Here we start at
    // the oxygen station and then search outward until we've exhausted all next
    // states.
    let mut oxygen_system_state = oxygen_system_state.unwrap();
    oxygen_system_state.num_steps = 0;
    visited.clear();
    visited.insert(oxygen_system_state.point.clone());

    search_states.clear();
    search_states.push_front(oxygen_system_state);

    let mut num_steps = 0;
    loop {
        let state = match search_states.pop_back() {
            Some(x) => x,
            None => break,
        };

        for d in &[Direction::North, Direction::East, Direction::South, Direction::West ] {
            match state.next(&visited, *d) {
                Some(next_state) => {
                    num_steps = next_state.num_steps;
                    visited.insert(next_state.point.clone());
                    search_states.push_front(next_state);
                },
                None => (),
            }
        }
    }
    println!("Part 2: filled after {} minutes", num_steps);

    Ok(())
}
