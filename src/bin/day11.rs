//! Solution to [Advent of Code 2019 Day 11](https://adventofcode.com/2019/day/11).

use advent_of_code_2019::intcode::{IntcodeComputer, IntcodeState};
use std::cmp;
use std::collections::HashMap;
use std::env::args_os;
use std::fs::File;
use std::io::{self, BufReader, BufRead};

#[derive(Debug)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Debug)]
struct PainterRobot {
    pub x: isize,
    pub y: isize,
    pub direction: Direction,
}

impl PainterRobot {
    pub fn new() -> Self {
        PainterRobot { x: 0, y: 0, direction: Direction::North }
    }

    pub fn rotate(&mut self, signal: isize) {
        self.direction = match self.direction {
            Direction::North => if signal == 0 { Direction::West }  else { Direction::East },
            Direction::East  => if signal == 0 { Direction::North } else { Direction::South },
            Direction::South => if signal == 0 { Direction::East }  else { Direction::West },
            Direction::West  => if signal == 0 { Direction::South } else { Direction::North },
        }
    }

    pub fn step(&mut self) {
        match self.direction {
            Direction::North => self.y -= 1,
            Direction::East  => self.x += 1,
            Direction::South => self.y += 1,
            Direction::West  => self.x -= 1,
        }
    }
}

fn paint(intcode_string: &str, starting_color: i64) -> HashMap<(isize, isize), usize> {
    let mut intcoder = IntcodeComputer::from_string_state_with_memory(intcode_string, 4096);
    intcoder.push_input(starting_color);  // all panels start off black so we'll frontload the robot's query

    let mut painter_robot = PainterRobot::new();
    let mut coords_to_color: HashMap<(isize, isize), usize> = HashMap::new();

    loop {
        intcoder.run_to_first_output();
        if intcoder.state == IntcodeState::Terminated {
            break;
        }
        let color = intcoder.pop_output().unwrap();
        intcoder.run_to_first_output();
        if intcoder.state == IntcodeState::Terminated {
            break;
        }
        let rotation = intcoder.pop_output().unwrap();

        coords_to_color.insert((painter_robot.x, painter_robot.y), color as usize);
        painter_robot.rotate(rotation as isize);
        painter_robot.step();
        intcoder.push_input(*coords_to_color.entry((painter_robot.x, painter_robot.y)).or_insert(0) as i64);  // TODO: we insert the black panel but it doesn't mean that our robot painted it (it could terminate the next step);
    }
    coords_to_color
}

fn print_painting(coords_to_color: HashMap<(isize, isize), usize>) {
    let min_xy = coords_to_color.keys().fold((std::isize::MAX, std::isize::MAX), |acc: (isize, isize), x: &(isize, isize)| (cmp::min(acc.0, x.0), cmp::min(acc.1, x.1)));
    let max_xy = coords_to_color.keys().fold((std::isize::MIN, std::isize::MIN), |acc: (isize, isize), x: &(isize, isize)| (cmp::max(acc.0, x.0), cmp::max(acc.1, x.1)));
    for y in min_xy.1..=max_xy.1 {
        for x in min_xy.0..=max_xy.0 {
            print!("{}", match coords_to_color.get(&(x, y)) { Some(1) => "#", _ => " " });
        }
        println!();
    }
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the input data will be loaded from the file specified by the first command-
/// line argument.
fn main() -> io::Result<()> {
    let f: Box<dyn BufRead> = match args_os().nth(1) {
        Some(input_filename) => Box::new(BufReader::new(File::open(input_filename)?)),
        None => Box::new(BufReader::new(&include_bytes!("../../data/day11/input.txt")[..])),
    };
    let intcode_string: String = f.lines().next().unwrap()?;

    let coords_to_color = paint(&intcode_string, 0);
    println!("Part 1: num panels painted: {}", coords_to_color.len());

    let coords_to_color = paint(&intcode_string, 1);
    println!("Part 2: output");
    print_painting(coords_to_color);

    Ok(())
}
