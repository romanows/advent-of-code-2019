//! Solution to [Advent of Code 2019 Day 4](https://adventofcode.com/2019/day/4).

// How many different passwords within the range 136760-595730?
//
// # PART 1
// Two adjacent digits are the same (like 22 in 122345).
// Going from left to right, the digits never decrease.
//
// Examples:
//    111111 meets these criteria (double 11, never decreases).
//    223450 does not meet these criteria (decreasing pair of digits 50).
//    123789 does not meet these criteria (no double).
//
// So the brute force method is probably quick enough.  If we wanted to go
// quicker, we could calculate by how much we'd have to skip to get to the next
// viable number since if it is not "1" then it'll be either the next number
// that produces a pair or the next number that is increasing, might as well
// jump ahead by whatever is larger.
//
// If we were going to use a solver, this would be super easy:
// Given ABCDEF
// A <= B <= C <= D <= E <= F
// A == B || B == C || C == D || D == E || E == F
// A * 10^5 + B * 10^4 + ... + F >= 136760
// (analog for upper bound)
//
// And we just count all solutions.
//
// # PART 2
// Additional rule is that there must be one pair of identical adjacent digits
// that does not have other additional identical digits on either side.

use std::env::args;
use itertools::Itertools;

#[allow(clippy::collapsible_if,clippy::many_single_char_names)]
fn is_valid(password: u32) -> bool {
    let mut ret = false;

    fn h(remainder: u32, divisor: u32) -> (u32, u32) {
        (remainder / divisor, remainder % divisor)
    }

    let (a, remainder) = h(password, 100_000);
    let (b, remainder) = h(remainder, 10000);
    if a <= b {
        let (c, remainder) = h(remainder, 1000);
        if b <= c {
            let (d, remainder) = h(remainder, 100);
            if c <= d {
                let (e, f) = h(remainder, 10);
                if d <= e {
                    if e <= f {
                        if a == b || b == c || c == d || d == e || e == f {
                            ret = true;
                        }
                    }
                }
            }
        }
    }
    ret
}

#[allow(clippy::collapsible_if,clippy::many_single_char_names,clippy::nonminimal_bool)]
fn is_valid_part2(password: u32) -> bool {
    let mut ret = false;

    fn h(remainder: u32, divisor: u32) -> (u32, u32) {
        (remainder / divisor, remainder % divisor)
    }

    let (a, remainder) = h(password, 100_000);
    let (b, remainder) = h(remainder, 10000);
    if a <= b {
        let (c, remainder) = h(remainder, 1000);
        if b <= c {
            let (d, remainder) = h(remainder, 100);
            if c <= d {
                let (e, f) = h(remainder, 10);
                if d <= e {
                    if e <= f {
                        if (a == b && b != c)
                                || (a != b && b == c && c != d)
                                || (b != c && c == d && d != e)
                                || (c != d && d == e && e != f)
                                || (d != e && e == f) {
                            ret = true;
                        }
                    }
                }
            }
        }
    }
    ret
}

/// Prints answers to part 1 and 2 to stdout.
///
/// When no command-line arguments are given, my input data is used; otherwise,
/// the lower and upper-bound data will be taken from the first two command-line
/// arguments.
#[allow(clippy::unreadable_literal)]
fn main() {
    let (lower, upper): (u32, u32) = match args().skip(1).take(2).map(|x| x.parse::<u32>().unwrap()).next_tuple() {
        Some((x, y)) => (x, y),
        _ => (136760, 595730),
    };
    let mut count = 0;
    let mut count_part2 = 0;
    for password in lower..=upper {
        if is_valid(password) {
            count += 1;
        }
        if is_valid_part2(password) {
            count_part2 += 1;
        }
    }
    println!("Part 1: number of valid passwords is {}", count);
    println!("Part 2: number of valid passwords is {}", count_part2);
}
