//! Intcode virtual machine implementation.
//!
//! Slowly built up over multiple days of the [Advent of Code 2019](https://adventofcode.com/2019).

extern crate num_integer;

use num_integer::div_rem;
use std::collections::VecDeque;
use std::convert::TryFrom;

// Note, I think this a better implementation:
// https://github.com/piyushrungta25/advent-of-code-2019/blob/master/day5/src/main.rs
//
// I like the use of Enums for the opcodes that allow the number of parameters
// to be typechecked.  My storing of opcodes in an array and directly fetching
// them is not as clever as I thought it would be.  The ParameterMode enums
// that carry the underlying parameter value are a clean way to pair the mode
// with the underlying data.
//
// What I get right: looking at a few of the Rust examples, I like how my
// source code includes tests from the problem statements!

#[derive(Clone)]
pub struct IntcodeComputer {
    /// Instruction pointer
    pub ip: usize,

    /// Relative base
    pub rb: i64,

    pub memory: Vec<i64>,

    pub state: IntcodeState,

    /// use push_front to load the input
    input: VecDeque<i64>,

    /// use pop_back to empty the output
    output: VecDeque<i64>,
}

enum ParameterMode {
    Position,
    Immediate,
    Relative,
}

impl ParameterMode {
    fn from_value(flag: usize) -> Self {
        match flag {
            0 => ParameterMode::Position,
            1 => ParameterMode::Immediate,
            2 => ParameterMode::Relative,
            _ => panic!(""),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum IntcodeState {
    Running,
    OutputAvailable,
    InputRequested,
    Terminated,
}

impl IntcodeComputer {
    /// Parse a comma-delimited list of integers that corresponds to the memory of
    /// an Intcode computer.
    pub fn from_string_state(s: &str) -> IntcodeComputer {
        let ip = 0;
        let rb = 0;
        let memory = s.split(',').map(|x| x.parse::<i64>().unwrap()).collect();
        IntcodeComputer { ip, rb, memory, state: IntcodeState::Running, input: VecDeque::new(), output: VecDeque::new() }
    }

    /// Parse a comma-delimited list of integers that corresponds to the memory of
    /// an Intcode computer.
    pub fn from_string_state_with_memory(s: &str, memory_size: usize) -> IntcodeComputer {
        let ip = 0;
        let rb = 0;
        let mut memory: Vec<i64> = s.split(',').map(|x| x.parse::<i64>().unwrap()).collect();
        memory.resize(memory_size, 0);
        IntcodeComputer { ip, rb, memory, state: IntcodeState::Running, input: VecDeque::new(), output: VecDeque::new() }
    }

    /// Run an Intcode computation until termination ignoring requests for input,
    /// which could lead to panics if sufficient input was not already queued up for
    /// the program.
    pub fn run(&mut self) {
        while self.state != IntcodeState::Terminated { self.step(false); };
    }

    /// Run an Intcode computation until termination or until output becomes
    /// available, ignoring requests for input, which could lead to panics if
    /// sufficient input was not already queued up for the program.  Will run if
    /// called even when output is available.
    pub fn run_to_first_output(&mut self) {
        while self.state != IntcodeState::Terminated {
            self.step(false);
            if self.state == IntcodeState::OutputAvailable {
                break;
            }
        }
    }

    pub fn run_to_first_input(&mut self) {
        let mut is_breaking_on_input = false;
        while self.state != IntcodeState::Terminated {
            self.step(is_breaking_on_input);
            is_breaking_on_input = true;
            if self.state == IntcodeState::InputRequested {
                break;
            }
        }
    }

    /// Run the next-step of an Intcode computation, returning `true` when the
    /// computation has not yet terminated and `false` when the program reaches a
    /// termination opcode.
    pub fn step(&mut self, is_breaking_on_input: bool) {
        if self.state != IntcodeState::Terminated {
            self.state = IntcodeState::Running;
            let opcode: usize = usize::try_from(self.memory[self.ip]).unwrap_or_else(|_| panic!("Illegal opcode value '{}' at instruction pointer offset '{}'", self.ip, self.memory[self.ip]));
            let (pm3, opcode) = div_rem(opcode, 10000);
            let (pm2, opcode) = div_rem(opcode, 1000);
            let (pm1, opcode) = div_rem(opcode, 100);
            if opcode == 99 {
                self.state = IntcodeState::Terminated;
            } else {
                let op = OPCODE_POINTER[opcode];
                let is_op_input = (op as usize) == (op_input as usize);
                if is_breaking_on_input && is_op_input {
                    self.state = IntcodeState::InputRequested;
                } else {
                    op(self, &[ParameterMode::from_value(pm1), ParameterMode::from_value(pm2), ParameterMode::from_value(pm3)]);
                }
            }
        }
    }

    pub fn push_input(&mut self, x: i64) {
        self.input.push_front(x);
    }

    pub fn pop_output(&mut self) -> Option<i64> {
        self.output.pop_back()
    }
}

/// Allows us to lookup an opcode from its integer representation.
const OPCODE_POINTER: [fn(&mut IntcodeComputer, &[ParameterMode; 3]) -> (); 10] = [op_illegal, op_sum, op_mul, op_input, op_output, op_jump_if_true, op_jump_if_false, op_less_than, op_equals, op_rb];

fn op_illegal(ic: &mut IntcodeComputer, _pm: &[ParameterMode; 3]) {
    panic!("Illegal opcode '{}'", ic.memory[ic.ip]);
}

fn usizer(x: i64) -> usize {
    usize::try_from(x).unwrap_or_else(|_| panic!("Memory address is not a valid memory position value: {}", x))
}

fn load(ic: &IntcodeComputer, pm: &[ParameterMode; 3], ip_offset: usize) -> i64 {
    match &pm[ip_offset - 1] {
        ParameterMode::Position  => ic.memory[usizer(ic.memory[ic.ip + ip_offset])],
        ParameterMode::Immediate => ic.memory[ic.ip + ip_offset],
        ParameterMode::Relative  => ic.memory[usizer(ic.rb + ic.memory[ic.ip + ip_offset])],
    }
}

fn store(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3], ip_offset: usize, value: i64) {
    match &pm[ip_offset - 1] {
        ParameterMode::Position  => {
            let output_idx = usizer(ic.memory[ic.ip + ip_offset]);
            ic.memory[output_idx] = value;
        },
        ParameterMode::Immediate => panic!("Illegal store to immediate mode parameter"),
        ParameterMode::Relative  => {
            let output_idx = usizer(ic.rb + ic.memory[ic.ip + ip_offset]);
            ic.memory[output_idx] = value;
        }
    }
}

fn op_sum(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    let x = load(ic, pm, 1) + load(ic, pm, 2);
    store(ic, pm, 3, x);
    ic.ip += 4;
}

fn op_mul(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    let x = load(ic, pm, 1) * load(ic, pm, 2);
    store(ic, pm, 3, x);
    ic.ip += 4;
}

/// Stores `ic.input` in `ic.memory`
fn op_input(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    let x = ic.input.pop_back().unwrap();
    store(ic, pm, 1, x);
    ic.ip += 2;
}

/// Writes `ic.memory` to `ic.output`
fn op_output(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    ic.output.push_front(load(ic, pm, 1));
    ic.ip += 2;
    ic.state = IntcodeState::OutputAvailable;
}

/// If the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter
fn op_jump_if_true(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    ic.ip = if load(ic, pm, 1) != 0 {
        usizer(load(ic, pm, 2))
    } else {
        ic.ip + 3
    }
}

/// If the first parameter is zero, it sets the instruction pointer to the value from the second parameter.
fn op_jump_if_false(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    ic.ip = if load(ic, pm, 1) == 0 {
        usizer(load(ic, pm, 2))
    } else {
        ic.ip + 3
    }
}

/// If the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter.
fn op_less_than(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    let x = if load(ic, pm, 1) < load(ic, pm, 2) { 1 } else { 0 };
    store(ic, pm, 3, x);
    ic.ip += 4
}

/// If the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter.
fn op_equals(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    let x = if load(ic, pm, 1) == load(ic, pm, 2) { 1 } else { 0 };
    store(ic, pm, 3, x);
    ic.ip += 4
}

fn op_rb(ic: &mut IntcodeComputer, pm: &[ParameterMode; 3]) {
    ic.rb += load(ic, pm, 1);
    ic.ip += 2;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_given_day2_part1() {
        let intcoder = IntcodeComputer::from_string_state("1,0,0,0,99");
        assert_eq!(intcoder.memory, vec![1, 0, 0, 0, 99]);

        // 1,0,0,0,99 becomes 2,0,0,0,99 (1 + 1 = 2).
        let mut intcoder = IntcodeComputer::from_string_state("1,0,0,0,99");
        intcoder.run();
        assert_eq!(intcoder.memory, vec![2, 0, 0, 0, 99]);

        // 2,3,0,3,99 becomes 2,3,0,6,99 (3 * 2 = 6).
        let mut intcoder = IntcodeComputer::from_string_state("2,3,0,3,99");
        intcoder.run();
        assert_eq!(intcoder.memory, vec![2, 3, 0, 6, 99]);

        // 2,4,4,5,99,0 becomes 2,4,4,5,99,9801 (99 * 99 = 9801).
        let mut intcoder = IntcodeComputer::from_string_state("2,4,4,5,99,0");
        intcoder.run();
        assert_eq!(intcoder.memory, vec![2, 4, 4, 5, 99, 9801]);

        // 1,1,1,4,99,5,6,0,99 becomes 30,1,1,4,2,5,6,0,99.
        let mut intcoder = IntcodeComputer::from_string_state("1,1,1,4,99,5,6,0,99");
        intcoder.run();
        assert_eq!(intcoder.memory, vec![30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }

    #[test]
    fn test_given_day5_part1() {
        let intcoder = IntcodeComputer::from_string_state("1,0,0,0,99");
        assert_eq!(intcoder.memory, vec![1, 0, 0, 0, 99]);

        // test negative numbers, like the first example from test_given_day2_part1, above
        let mut intcoder = IntcodeComputer::from_string_state("1001,0,-1002,0,99");
        intcoder.run();
        assert_eq!(intcoder.memory, vec![-1, 0, -1002, 0, 99]);

        // test from the problem description
        let mut intcoder = IntcodeComputer::from_string_state("1101,100,-1,4,0");
        intcoder.run();
        assert_eq!(intcoder.memory, vec![1101, 100, -1, 4, 99]);

        // outputs whatever it gets as input, then halts
        let mut intcoder = IntcodeComputer::from_string_state("3,0,4,0,99");
        intcoder.push_input(42);
        assert_eq!(intcoder.pop_output(), None);
        intcoder.run();
        assert_eq!(intcoder.pop_output(), Some(42));

        // 1002,4,3,4,33 is 33 * 3 = 99 written to address 4 to become 1002,4,3,99
        let mut intcoder = IntcodeComputer::from_string_state("1002,4,3,4,33");
        intcoder.run();
        assert_eq!(intcoder.memory, vec![1002, 4, 3, 4, 99]);
    }

    #[test]
    fn test_given_day5_part2() {
        fn test(program: &str, input: i64, output: i64) {
            let mut intcoder = IntcodeComputer::from_string_state(program);
            intcoder.push_input(input);
            intcoder.run();
            assert_eq!(intcoder.pop_output(), Some(output));
        }

        // Using position mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).
        let program = "3,9,8,9,10,9,4,9,99,-1,8";
        test(program, 8, 1);
        test(program, 7, 0);

        // Using position mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).
        let program = "3,9,7,9,10,9,4,9,99,-1,8";
        test(program, 8, 0);
        test(program, 7, 1);

        // Using immedate mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).
        let program = "3,3,1108,-1,8,3,4,3,99";
        test(program, 8, 1);
        test(program, 7, 0);

        // Using immediate mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).
        let program = "3,3,1107,-1,8,3,4,3,99";
        test(program, 8, 0);
        test(program, 7, 1);

        // Here are some jump tests that take an input, then output 0 if the input was zero or 1 if the input was non-zero.
        // using position mode
        let program = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9";
        test(program, 0, 0);
        test(program, -42, 1);

        // using immediate mode
        let program = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1";
        test(program, 0, 0);
        test(program, -42, 1);

        // The program will then output 999 if the input value is below 8, output 1000 if the input value is equal to 8, or output 1001 if the input value is greater than 8.
        let program = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99";
        test(program, 7, 999);
        test(program, 8, 1000);
        test(program, 9, 1001);
    }

    #[test]
    fn test_given_rb_add() {
        let program = "109,19,99";
        let mut intcoder = IntcodeComputer::from_string_state(program);
        intcoder.rb = 2000;
        intcoder.run();
        assert_eq!(intcoder.rb, 2019);
    }

    #[test]
    fn test_given_rb_sub() {
        let mut program_vec: Vec<i64> = Vec::with_capacity(2000);
        program_vec.resize(2000, 0);
        program_vec[0] = 204;
        program_vec[1] = -34;
        program_vec[2] = 99;
        program_vec[2000 - 34] = 42;
        let program = program_vec.iter().map(|x| x.to_string()).collect::<Vec<String>>().join(",");
        let mut intcoder = IntcodeComputer::from_string_state(&program);
        intcoder.rb = 2000;
        intcoder.run();
        assert_eq!(intcoder.pop_output().unwrap(), 42);

    }

    #[test]
    fn test_given_day9_part1() {
        // takes no input and produces a copy of itself as output
        let program = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
        let mut intcoder = IntcodeComputer::from_string_state_with_memory(program, 128);
        intcoder.run();
        let mut outputs: Vec<String> = Vec::new();
        loop {
            if let Some(x) = intcoder.pop_output() {
                outputs.push(x.to_string());
            } else {
                break;
            }
        }
        assert_eq!(outputs.join(","), program);

        //  should output a 16-digit number
        let program = "1102,34915192,34915192,7,4,7,99,0";

        // should output the large number in the middle
        let program = "104,1125899906842624,99";
        let mut intcoder = IntcodeComputer::from_string_state(program);
        intcoder.run();
        assert_eq!(intcoder.pop_output().unwrap(), 1125899906842624);
    }
}
